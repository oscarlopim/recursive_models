from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F 
from rnn_stacks import get_rnn_stack
from tinyFlame.models import add_model_info, DummyModule, MLP, generate_tensor_mask_last_step

#####################################################################

def get_cell_ajust_factor(cell_name):
	if cell_name == 'RNN':
		return 1
	elif cell_name == 'GRU':
		return 1 # 3 OJO
	elif cell_name == 'LSTM':
		return 4
	elif cell_name == 'PLSTM':
		return 4
	else:
		raise Exception('cell not supported')

class RNN_lightCurves(nn.Module):
	def __init__(self, model_parameters, use_as_feature_extractor=False):
		super().__init__()
		self.model_parameters = model_parameters

		# ATTRIBUTES

		self.input_dim = model_parameters['input_dim']
		self.rnn_units = model_parameters['rnn_units']
		self.rnn_layers = model_parameters['rnn_layers']
		self.uses_batchnorm = model_parameters['uses_batchnorm']
		self.bi_lstm = bool(model_parameters['bi_lstm'])
		self.output_dim = model_parameters['n_classes']
		self.rnn_class = model_parameters['rnn_class']
		self.max_curve_length = model_parameters['max_curve_length'] # used for optimization
		self.use_as_feature_extractor = use_as_feature_extractor
		self.residual = model_parameters['residual']
		self.dropout = model_parameters['dropout']

		# DROPOUT AND RESIDUAL
		
		if self.residual:
			raise Warning('you are using residual on RNN')
			self.residual_dropout = self.dropout
			self.residual_fc = nn.Linear(self.input_dim, rnn_units)
			print('residual_fc',self.residual_fc)

		# RNN STACK

		self.rnn_stack = get_rnn_stack(self.rnn_class, self.input_dim, self.rnn_units, self.rnn_layers, self.dropout,
			self.bi_lstm, self.uses_batchnorm, self.max_curve_length)
		print('rnn_stack:', self.rnn_stack)
		self.model_output_dim = self.rnn_units

		# LAST MLP

		if self.use_as_feature_extractor:
			self.last_mlp = DummyModule()
		else:
			self.define_last_mlp(self.rnn_units, self.rnn_layers, self.output_dim, self.rnn_class, self.dropout)
		print('last_mlp:', self.last_mlp)

		# OTHERS

		self.use_days = (self.rnn_class=='PLSTM')

		# NAME

		self.get_name()

	def define_last_mlp(self, mlp_dim, rnn_layers, output_dim, rnn_class, dropout):
		cell_ajust_factor = get_cell_ajust_factor(rnn_class)
		hidden_units = []
		hidden_units = [mlp_dim*cell_ajust_factor, mlp_dim]
		activation = 'relu'
		last_activation = 'softmax'

		self.last_mlp = MLP(mlp_dim, output_dim, hidden_units, activation=activation, dropout=dropout, last_activation=last_activation)
		self.model_output_dim = output_dim

	def get_output_dims(self):
		return self.model_output_dim

	def get_name(self):
		mp = self.model_parameters

		name = ''
		if mp['rnn_class']=='PLSTM':
			name += add_model_info('mdl','PLSTM')
		else:
			name += add_model_info('mdl',mp['rnn_class'])
		name += add_model_info('L',mp['rnn_layers'])
		name += add_model_info('U',mp['rnn_units'])

		if mp['bi_lstm']:
			name += add_model_info('bi',1)
		if mp['uses_batchnorm']:
			name += add_model_info('bn',1)

		self.name = name[1:] # delete first atribute!! -> -
	
	def forward(self, x, x_lengths, other_args):
		x, info = self.forward_recurrent(x, x_lengths, other_args)
		x = self.forward_final(x)

		# GET LAST
		last_step_mask = generate_tensor_mask_last_step(x_lengths, x.shape[1], x.device).unsqueeze(-1) # (bxtx1)
		last_x = x.masked_fill(last_step_mask==0, 0).sum(dim=1)

		plot = 0
		if plot:
			print('x_lengths',x_lengths.shape,x_lengths[0])
			print('x',x.shape,x[0])
			print('last_x',last_x.shape,last_x[0])

		return x, last_x, info

	def forward_recurrent(self, x, x_lengths, other_args):
		x_lengths[x_lengths==0] = 1 # PATCH FOR MULTIBAND
		#x = self.testing(x, onehots)

		other_args = []
		initial_x = x
		x, hidden, info = self.rnn_stack(x, x_lengths, *other_args) # out, (ht, ct)

		if self.residual: # RESIDUAL
			x = self.residual_fc(x)
			x = self.residual_dropout(x)
			x = x + initial_x

		return x, info
		#return x, last_ht, info

	def forward_final(self, x):
		y_pred = self.last_mlp(x)
		return y_pred

	def capture_grad(self):
		self.rnn_stack.capture_grad()

	def capture_grad_end(self):
		self.rnn_stack.capture_grad_end()
		
