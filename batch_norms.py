from __future__ import print_function
from __future__ import division

import math

import torch
import torch.nn as nn
import torch.nn.functional as F 

class DummyBatchNorm(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, *args, **kwargs):
		super().__init__()

	def forward(self, x, mask):
		x = x.masked_fill(~mask, 0)
		return x

class BatchNorm1d_proxy(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, num_features, eps=1e-5, momentum=0.1, track_running_stats=True, **kwargs):
		super().__init__()
		self.bnorm = nn.BatchNorm1d(num_features=num_features, eps=eps, momentum=momentum, track_running_stats=track_running_stats)

	def forward(self, x, mask):
		x = x.masked_fill(~mask, 0)
		x = self.bnorm(x)
		x = x.masked_fill(~mask, 0)
		return x

class LayerNorm_proxy(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, normalized_shape, eps=1e-5, **kwargs):
		super().__init__()
		max_curve_length = 60
		self.bnorm = nn.LayerNorm(normalized_shape=(normalized_shape), eps=eps)

	def forward(self, x, mask):
		#return x # DUMMY
		x = x.masked_fill(~mask, 0)
		#x = x.transpose(-2,-1)
		x = self.bnorm(x)
		#x = x.transpose(-2,-1)
		x = x.masked_fill(~mask, 0)
		return x

class BatchNormMask(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, num_features, eps=1e-5, momentum=0.1, track_running_stats=True, time_last_dim=False):
		super().__init__()
		self.a_2 = nn.Parameter(torch.ones(num_features))
		self.b_2 = nn.Parameter(torch.zeros(num_features))
		self.eps = eps
		self.momentum = momentum
		self.track_running_stats = True
		self.affine = True
		self.time_last_dim = time_last_dim

		self.register_buffer('running_mean', torch.zeros(num_features))
		self.register_buffer('running_var', torch.ones(num_features))
		self.register_buffer('num_batches_tracked', torch.tensor(0, dtype=torch.long))
		self.reset_parameters()

	def reset_running_stats(self):
		if self.track_running_stats:
			self.running_mean.zero_()
			self.running_var.fill_(1)
			self.num_batches_tracked.zero_()

	def reset_parameters(self):
		self.reset_running_stats()

	def get_running_stats(self, x, mask):
		'''
		x (bxfxt)
		mask (bx1xt)
		'''

		if self.time_last_dim: # x (bxfxt)
			#print('x',x.shape,x[0,0,:])
			#print('mask', mask.shape,mask[0,0,:])
			x_sum = (x.masked_fill(mask==0, 0)).sum(dim=0).sum(dim=-1) # (f)
			mask_sum = mask.sum(dim=0).sum(dim=-1) # (1)
			#print('mask_sum',mask_sum.shape,mask_sum[0])
			sample_mean = x_sum / mask_sum # (f)
			#print('sample_mean',sample_mean.shape)
			x_mean = x-sample_mean.unsqueeze(0).unsqueeze(-1)
			#sample_var = torch.sqrt(((x_mean*mask)**2).sum(dim=0).sum(dim=-1) / mask_sum) # (f)
			sample_var = ((x_mean*mask)**2).sum(dim=0).sum(dim=-1) / mask_sum # (f)
			#print('sample_var',sample_var.shape)

		else: # x (bxtxf)
			#print('x',x.shape,x[0,:,0])
			#print('mask', mask.shape,mask[0,:,0])
			#print('x*mask',x.shape,(x*mask)[0,:,0])
			x_sum = (x.masked_fill(mask==0, 0)).sum(dim=0).sum(dim=0) # (f)
			mask_sum = mask.sum(dim=0).sum(dim=0) # (1)
			#print('mask_sum',mask_sum.shape,mask_sum[0])
			sample_mean = x_sum / mask_sum # (f)
			#print('sample_mean',sample_mean.shape)
			x_mean = x-sample_mean.unsqueeze(0).unsqueeze(0)
			#sample_var = torch.sqrt(((x_mean*mask)**2).sum(dim=0).sum(dim=-1) / mask_sum) # (f)
			sample_var = ((x_mean.masked_fill(mask==0, 0))**2).sum(dim=0).sum(dim=0) / mask_sum # (f)
			#print('sample_var',sample_var.shape)

		self.get_running_stats_end(sample_mean, sample_var)

	def get_running_stats_end(self, sample_mean, sample_var):
		self.running_mean = self.momentum * self.running_mean + (1 - self.momentum) * sample_mean
		self.running_var = self.momentum * self.running_var + (1 - self.momentum) * sample_var

	def forward(self, x, mask):
		'''
		x (bxfxt)
		mask (bx1xt)
		time_last_dim
		'''

		if self.training:
			with torch.no_grad():
				self.get_running_stats(x, mask)

		#print('x',x[0,0,:])
		if self.time_last_dim:
			a_2 = self.a_2.unsqueeze(0).unsqueeze(-1)
			b_2 = self.b_2.unsqueeze(0).unsqueeze(-1)
			running_mean = self.running_mean.unsqueeze(0).unsqueeze(-1)
			running_var = self.running_var.unsqueeze(0).unsqueeze(-1)
		else:
			a_2 = self.a_2.unsqueeze(0).unsqueeze(0)
			b_2 = self.b_2.unsqueeze(0).unsqueeze(0)
			running_mean = self.running_mean.unsqueeze(0).unsqueeze(0)
			running_var = self.running_var.unsqueeze(0).unsqueeze(0)

		x = x.masked_fill(mask==0, 0)
		x = a_2 * (x - running_mean) / torch.sqrt((running_var + self.eps)) + b_2
		#x = a_2 * (x - running_mean) / (running_var + self.eps) + b_2
		x = x.masked_fill(mask==0, 0)
		#print('x',x[0,0,:])
		return x
