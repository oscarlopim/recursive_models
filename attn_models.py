from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F 
from tinyFlame.models import MLP, add_model_info, generate_tensor_mask_last_step
from attn_stacks import DummyModule, AttnStackClassic
import numpy as np

#####################################################################

class ATTN_lightCurves(nn.Module):
	def __init__(self, model_parameters, use_as_feature_extractor=False):
		super().__init__()
		self.model_parameters = model_parameters

		# ATTRIBUTES

		self.input_dim = model_parameters['input_dim']
		self.rnn_units = model_parameters['rnn_units']
		self.rnn_layers = model_parameters['rnn_layers']
		self.uses_batchnorm = model_parameters['uses_batchnorm']
		self.bi_lstm = bool(model_parameters['bi_lstm'])
		self.output_dim = model_parameters['n_classes']
		self.max_curve_length = model_parameters['max_curve_length'] # used for optimization
		self.use_as_feature_extractor = use_as_feature_extractor
		self.residual = model_parameters['residual']
		self.dropout = model_parameters['dropout']
		self.along_curve_dropout = model_parameters['along_curve_dropout']
		self.uses_attention = model_parameters['uses_attention']
		
		# ATTN

		self.kernel_size = 5
		self.attn_heads = 4

		if not self.uses_attention:
			self.attn_heads = 0

		# DROPOUT AND RESIDUAL
		
		if self.residual:
			self.residual_dropout = self.dropout
			self.residual_fc = nn.Linear(self.input_dim, rnn_units)
			print('residual_fc:',self.residual_fc)

		# ATTN STACK

		self.attn_stack = AttnStackClassic(self.input_dim, self.rnn_units, self.rnn_layers, self.dropout, self.uses_batchnorm, self.max_curve_length,
			kernel_size=self.kernel_size, attn_heads=self.attn_heads, along_curve_dropout=self.along_curve_dropout)
		print('attn_stack:',self.attn_stack)
		self.model_output_dim = self.rnn_units

		# LAST MLP

		if self.use_as_feature_extractor:
			self.last_mlp = DummyModule(self.rnn_units)
		else:
			self.define_last_mlp(self.rnn_units, self.rnn_layers, self.output_dim, self.dropout)
		print('last_mlp:', self.last_mlp)

		# NAME

		self.get_name()

	def define_last_mlp(self, mlp_dim, rnn_layers, output_dim, dropout):
		hidden_units = []
		hidden_units = [] # CUSTOM IN ATTN
		activation = 'relu'
		last_activation = 'softmax'

		self.last_mlp = MLP(mlp_dim, output_dim, hidden_units, activation=activation, dropout=dropout, last_activation=last_activation)
		self.model_output_dim = output_dim

	def get_output_dims(self):
		return self.model_output_dim

	def get_name(self):
		'''
		name = ''

		if mp['use_batchnorm']:
			name += add_model_info('bn',1)
		'''
		mp = self.model_parameters
		name = '_mdl-selfAttn'
		name += add_model_info('Bcnn',mp['rnn_layers'])
		name += add_model_info('Ucnn',mp['rnn_units'])
		name += add_model_info('kernel',self.kernel_size)

		self.name = name[1:] # delete first atribute!! -> -

	def forward(self, x, x_lengths, other_args):
		x, info = self.forward_recurrent(x, x_lengths, other_args)
		#print('x',x[0,:,0])
		x = self.forward_final(x)

		# GET LAST
		last_step_mask = generate_tensor_mask_last_step(x_lengths, x.shape[1], x.device).unsqueeze(-1) # (bxtx1)
		last_x = x.masked_fill(last_step_mask==0, 0).sum(dim=1)

		plot = 0
		if plot:
			print('x_lengths',x_lengths.shape,x_lengths[0])
			print('x',x.shape,x[0])
			print('last_x',last_x.shape,last_x[0])

		return x, last_x, info

	def forward_recurrent(self, x, x_lengths, other_args):
		#print('x',x.shape)
		x_lengths[x_lengths==0] = 1 # PATCH FOR MULTIBAND
		#x = self.testing(x, onehots)

		other_args = []
		initial_x = x
		x, info = self.attn_stack(x, x_lengths, *other_args) # out, (ht, ct)

		if self.residual: # RESIDUAL
			x = self.residual_fc(x)
			x = self.residual_dropout(x)
			x = x + initial_x

		return x, info

	def forward_final(self, x):
		y_pred = self.last_mlp(x)
		return y_pred

	def capture_grad(self):
		self.bands_modules[0].capture_grad()

	def capture_grad_end(self):
		self.bands_modules[0].capture_grad_end()