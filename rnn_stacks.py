from __future__ import print_function
from __future__ import division

import torch
from torch.autograd import Function
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn_utils
import torch.nn as nn
from batch_normalization_LSTM import BNLSTMCell, LSTM
from tinyFlame.models import DummyModule, dummy_f

def get_cell_norm_dic(parameters):
	#norms = [None, None]
	norms = {'w':None, 'b':None}
	for p in parameters:
		p_grad = p.grad
		#print('p_grad',p_grad)
		if p_grad is not None:
			#print('p_grad',p_grad.shape)
			p_shape = p_grad.shape
			p_grad_norm = torch.norm(p_grad).item()
			if len(p_shape)>1: # is W
				norms['w'] = p_grad_norm
			else: # is B
				norms['b'] = p_grad_norm

	#print('-'*40)
	return norms # (w,b)

class RNN(torch.nn.RNN): # auxiliar for normal RNN but with relu
	def __init__(self, *args, **kwargs):
		nonlinearity = 'relu'
		kwargs['nonlinearity'] = nonlinearity
		#torch.nn.RNN.__init__(*args, **kwargs)
		super(RNN, self).__init__(*args, **kwargs)

def get_rnn_cell(cell_name):
	if cell_name=='GRU':
		return torch.nn.GRU
	if cell_name=='LSTM':
		return torch.nn.LSTM
	if cell_name=='RNN':
		#return RNN # relu
		return torch.nn.RNN # tanh
	if cell_name=='PLSTM':
		return torch.nn.LSTMCell #PLSTM
	raise Exception('No RNN class with this name: {}'.format(cell_name))

def GRU_hidden_transform(hidden):
	return (hidden, None)

class RNN_stack(nn.Module):
	'''
	here the RNN stack is created with the RNN_cell
	'''
	def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi, max_curve_length):
		super().__init__()

		# ATTRIBUTES

		self.hidden_size = output_dim//(1+int(bi))
		self.num_layers = num_layers
		self.dropout = dropout
		self.max_curve_length = max_curve_length
		RNN_cell = get_rnn_cell(cell_name) # get the RNN cell
		print('RNN_cell:',cell_name)

		# EXTRA INFO

		self.cumulated_info = {'grad_norm':{'w':[],'b':[]}}
		self.info = {'grad_norm':{'w':0,'b':0}}

		# MODULES

		self.stack = RNN_cell(input_size=input_features, hidden_size=self.hidden_size, num_layers=self.num_layers, dropout=self.dropout,
			bidirectional=bool(bi), batch_first=True)

		self.hidden_transform = dummy_f
		if cell_name=='GRU' or cell_name=='RNN':
			self.hidden_transform = GRU_hidden_transform

	def forward(self, x, x_lengths):
		'''
		General RNN forward

		Parameters
		----------
		x (tensor)(<b,t,x_in>): input tensor.
		x_lengths(tensor)(<b,>): input curve lengths.

		Return
		----------
		x_out (tensor)(<b,t,h>): output tensor.
		hidden (tensor tuple (h,c) per layer)((<l,b,h>,<l,b,c>)): tuple of hidden spaces (h,c) in the last curve step (t=curve.length).
									h is None if the cell is another than LSTM.
		'''

		#print('x',x.shape)
		pack = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # argument is tensor
		#print('pack',pack)
		x, hidden = self.stack(pack) # (ht, ct)
		x,_ = nn.utils.rnn.pad_packed_sequence(x, batch_first=True, padding_value=0, total_length=int(self.max_curve_length)) # argument is Sequence
		
		## FORWARD END
		hidden = self.hidden_transform(hidden)
		return x, hidden, self.info

	def capture_grad(self): # called only in training
		norm_dic = get_cell_norm_dic(self.stack.parameters())
		self.cumulated_info['grad_norm']['w'].append(norm_dic['w'])
		self.cumulated_info['grad_norm']['b'].append(norm_dic['b'])
		#print('grad_norm',[len(self.cumulated_info['grad_norm'][k]) for k in self.cumulated_info['grad_norm']],'self.training',self.training)


	def capture_grad_end(self): # called at training end
		self.info['grad_norm']['w'] = sum(self.cumulated_info['grad_norm']['w'])/len(self.cumulated_info['grad_norm']['w']) # ugly
		self.info['grad_norm']['b'] = sum(self.cumulated_info['grad_norm']['b'])/len(self.cumulated_info['grad_norm']['b']) # ugly
		self.cumulated_info['grad_norm']['w'] = []
		self.cumulated_info['grad_norm']['b'] = []

EPS = 1e-3

def phi(times, s, tau):
	tt = times-s
	x = torch.div(tt,tau)
	x = torch.floor(x)
	x2 = tau*x
	x3 = tt-x2
	return torch.div(x3, tau)

def time_gate_fast(phase, r_on, training_phase):
	leak_rate = 0.001
	if not training_phase:
		leak_rate = 0.0

	cond1 =torch.where(torch.le(phase, 0.5*r_on) ,  2.0*phase / r_on, torch.zeros_like(phase))
	cond2 = torch.where((torch.gt(phase, 0.5*r_on) ) & (torch.lt(phase,r_on) ),  2.0 - 2.0*phase / r_on, torch.zeros_like(phase))   
	cond3 = torch.where(torch.ge(phase,r_on) , leak_rate * phase, torch.zeros_like(phase))

	term = cond1+cond2+cond3
	return term

class PLSTM_stack(nn.Module):
	'''
	here the PLSTM stack
	'''
	def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi):
		super().__init__()
		self.num_layers = num_layers
	   
		print('RNN_cell:',cell_name) # PSLTM
		RNN_cell = get_rnn_cell(cell_name) # get the RNN cell
		hidden_size = output_dim//(1+int(bi))
		self.input_size = input_features
		self.hidden_size = hidden_size

		self.stack = nn.ModuleList()
		self.after_stack = nn.ModuleList()
		for k in range(num_layers):
			self.stack.append(RNN_cell(input_size=input_features, hidden_size=hidden_size))
			self.after_stack.append(nn.Dropout(dropout))
			input_features = hidden_size

		self.after_stack[-1] = DummyModule()
		self.cumulated_info = {'grad_norm':{'w':[],'b':[]}}
		self.info = {'grad_norm':{'w':0,'b':0}}

		############ INIT VARIABLES

		#tau = nn.Parameter(torch.FloatTensor(bt,max_curve_length )) 
		self.tau = nn.Parameter(torch.FloatTensor(num_layers,self.hidden_size))
		torch.exp(torch.nn.init.uniform_(self.tau, a=0.0, b=6))

		self.r_on = nn.Parameter(torch.FloatTensor(num_layers,self.hidden_size))
		#r_on = nn.Parameter(torch.FloatTensor(bt,max_curve_length ))
		nn.init.constant_(self.r_on, 0.05)

		#s = nn.Parameter(torch.FloatTensor(bt,max_curve_length ))
		self.s = nn.Parameter(torch.FloatTensor(num_layers,self.hidden_size))
		torch.nn.init.uniform_(self.s, a=0.0, b=6)

	def forward(self, x, x_lengths, days):
		#print('x',x[0,:,0],x_lengths[0])
		x_seq_as_q = rnn_utils.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False)
		x_seq_as_q, batch_sizes, sorted_indices, unsorted_indices = x_seq_as_q

		days_seq_as_q = rnn_utils.pack_padded_sequence(days, x_lengths, batch_first=True, enforce_sorted=False)[0]

		max_batch_size = int(batch_sizes[0])
		#x_last = torch.zeros((max_batch_size,x.shape[-1]), device=x.device)
		x_out = torch.zeros((max_batch_size, x.shape[1], self.hidden_size), device=x.device)
		hx_last = torch.zeros((self.num_layers, max_batch_size, self.hidden_size), device=x.device)
		cx_last = torch.zeros((self.num_layers, max_batch_size, self.hidden_size), device=x.device)

		batch_size_ = 0
		#print('batch_sizes',batch_sizes)
		for k, batch_size in enumerate(batch_sizes):
			#print('k',k)
			#print('batch_size',batch_size)
			x_ = x_seq_as_q[batch_size_:batch_size_+batch_size] # <b_,f>

			days_ = days_seq_as_q[batch_size_:batch_size_+batch_size] # <b_,1>
			#print('days_',days_.shape)
			#time = x_[:,0].unsqueeze(-1).expand(-1, self.hidden_size)
			time = days_.unsqueeze(-1).expand(-1, self.hidden_size)
			#print('time',time.shape)

			for c,(cell,after) in enumerate(zip(self.stack, self.after_stack)):
				hx = hx_last[c,:batch_size].clone()
				cx = cx_last[c,:batch_size].clone()
				#print('hx',hx.shape,'cx',cx.shape)

				hx_cell, cx_cell = cell(x_,(hx, cx))
				hx_cell = after(hx_cell)
				#print('x_',x_.shape,'hx',hx.shape,'cx',cx.shape)

				tau_pos = torch.clamp(self.tau[c], min=EPS)
				r_on_pos = torch.clamp(self.r_on[c], min=EPS)

				phase = phi(time, self.s[c], tau_pos)
				kappa = time_gate_fast(phase, r_on_pos, self.training)

				hx_final = kappa*hx_cell+(1-kappa)*hx
				cx_final = kappa*cx_cell+(1-kappa)*cx

				hx_last[c,:batch_size,:] = hx_final
				cx_last[c,:batch_size,:] = cx_final
				x_ = hx_final

			x_out[:batch_size,k,:] = x_

			batch_size_ += batch_size

		x_out = x_out[unsorted_indices]
		hx_last = hx_last[:,unsorted_indices]
		cx_last = cx_last[:,unsorted_indices]
		
		#print('x_out',x_out.shape,'hx_last',hx_last.shape,'cx_last',cx_last.shape)
		#print('-'*80)
		return x_out, (hx_last, cx_last), self.info

	def capture_grad(self): # called only in training
		norm_dic = get_cell_norm_dic(self.stack.parameters())
		self.cumulated_info['grad_norm']['w'].append(norm_dic['w'])
		self.cumulated_info['grad_norm']['b'].append(norm_dic['b'])
		#print('grad_norm',[len(self.cumulated_info['grad_norm'][k]) for k in self.cumulated_info['grad_norm']],'self.training',self.training)


	def capture_grad_end(self): # called at training end
		self.info['grad_norm']['w'] = sum(self.cumulated_info['grad_norm']['w'])/len(self.cumulated_info['grad_norm']['w']) # ugly
		self.info['grad_norm']['b'] = sum(self.cumulated_info['grad_norm']['b'])/len(self.cumulated_info['grad_norm']['b']) # ugly
		self.cumulated_info['grad_norm']['w'] = []
		self.cumulated_info['grad_norm']['b'] = []


def get_rnn_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi, use_batchnorm, max_curve_length):
	if cell_name=='PLSTM':
		return PLSTM_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi)

	if use_batchnorm: # it's an exception because is a custom class
		if cell_name!='LSTM':
			raise Exception('BN is only supported with LSTM, not with',cell_name)
		return BN_LSTM_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi, max_curve_length)

	else:
		return RNN_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi, max_curve_length) # get RNN stack!

#########################################

class BN_LSTM_stack(nn.Module):
	def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi, max_curve_length):
		#super(RNN_stack, self).__init__()
		super().__init__()
		if bi:
			raise Exception('not supported')

		self.rnn_stack = nn.ModuleList()
		input_size=input_features
		for i in range(num_layers):
			#print('i',i)
			rnn_cell = LSTM(cell_class=BNLSTMCell, input_size=input_size, hidden_size=output_dim, batch_first=True, max_length=max_curve_length)
			input_size = output_dim
			self.rnn_stack.append(rnn_cell)
		self.dropout = nn.Dropout(dropout)

	def forward(self, x, x_lengths):
		#print('in x',x.shape)
		for k,cell in enumerate(self.rnn_stack):
			x, hidden = cell(x, length=x_lengths) # (ht, ct)
			if k<len(self.rnn_stack)-1:
				#print(k)
				x = self.dropout(x)
		#print('out x',x.shape)

		return x, hidden