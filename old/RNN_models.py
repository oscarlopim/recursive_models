from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F 
from batch_normalization_LSTM import BNLSTMCell, LSTM
from rnn_utils import FinalLinear, get_RNNcell
from tinyFlame.models import add_model_info

#####################################################################

class RNN_lightCurves(nn.Module):
	def __init__(self, model_parameters):
		super(RNN_lightCurves, self).__init__()

		self.model_parameters = model_parameters
		input_dim = model_parameters['input_dim']
		latent_dim = model_parameters['latent_dim']
		self.bi_lstm = model_parameters['bi_lstm']
		num_classes = model_parameters['n_classes']
		self.layers = model_parameters['latent_layers']-1
		self.use_batchnorm = model_parameters['use_batchnorm']
		output_dim = num_classes
		dropout = model_parameters['dropout']
		self.dropout = nn.Dropout(dropout)
		self.rnn_class = model_parameters['rnn_class']
		RNN_CELL = get_RNNcell(self.rnn_class)

		# generate batchnorm layers
		if self.use_batchnorm:
			self.batchnorm_l = nn.ModuleList()
			self.batchnorm_l.append(nn.BatchNorm1d(input_dim-1))
			for i in range(self.layers):
				self.batchnorm_l.append(nn.BatchNorm1d(latent_dim))

		# generate RNN layers
		self.stack_rnn = nn.ModuleList()
		if self.use_batchnorm:
			if self.rnn_class!='LSTM':
				raise Exception('no suported with this cell:',self.rnn_class)
			hidden_rnn = LSTM(cell_class=BNLSTMCell, input_size=input_dim-1, hidden_size=latent_dim, batch_first=True, max_length=10)
		else:
			hidden_rnn = RNN_CELL(input_size=input_dim-1,hidden_size=latent_dim//(1+self.bi_lstm),num_layers=1,batch_first=True,dropout=0,bidirectional=self.bi_lstm==1)
		self.stack_rnn.append(hidden_rnn)

		for i in range(self.layers):
			if self.use_batchnorm:
				hidden_rnn = LSTM(cell_class=BNLSTMCell, input_size=latent_dim, hidden_size=latent_dim, batch_first=True, max_length=10)
			else:
				hidden_rnn = RNN_CELL(input_size=latent_dim,hidden_size=latent_dim//(1+self.bi_lstm),num_layers=1,batch_first=True,dropout=0,bidirectional=self.bi_lstm==1)
			self.stack_rnn.append(hidden_rnn)

		''' old
		self.stack_lstm2 = nn.LSTM(input_size=latent_dim, hidden_size=latent_dim, num_layers=layers,
								batch_first=True, dropout=0.1, bidirectional=self.bi_lstm==1) # 0.25 # stacked
		'''
		print('latent',latent_dim)
		self.last_fc = FinalLinear(latent_dim, output_dim, dropout)
		self.get_name()

	def print(self):
		print('model_parameters:',self.model_parameters)

	def get_name(self):
		mp = self.model_parameters

		name = ''
		name += add_model_info('mdl','vanilla'+mp['rnn_class'])
		name += add_model_info('ch',''.join(mp['channel_names']))
		#name += add_model_info('dout',mp['dropout'])
		name += add_model_info('L',mp['latent_layers'])
		name += add_model_info('U',mp['latent_dim'])

		if mp['bi_lstm']:
			name += add_model_info('bi',1)
		if mp['use_batchnorm']:
			name += add_model_info('bn',1)

		name = name[1:] # delete first atribute!! -> -
		self.name = name

	def apply_batchnorm(self, x, index):
		if self.use_batchnorm:
			x = x.contiguous() # necesario. No se por que
			x_view = x.view(x.shape[0],x.shape[2],x.shape[1])
			#print(x.shape)
			#print(x_view.shape)
			x = self.batchnorm_l[index](x_view)
			x = x.view(x.shape[0],x.shape[2],x.shape[1])
		return x

	def generate_packing_object(self, x, x_lengths):
		if self.packing:
			x = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # argument is tensor
		return x

	def generate_tensor_object(self, x, x_lengths):
		if self.packing:
			x,_ = nn.utils.rnn.pad_packed_sequence(x, batch_first=True) # argument is Sequence
		return x, x_lengths

	def forward(self, x_and_lengths):
		x_and_lengths = x_and_lengths.float()
		x = x_and_lengths[:,:,:-1]
		#x_lengths = x_and_lengths[:,0,-1].cpu().numpy() # NUMPY
		x_lengths = x_and_lengths[:,0,-1].long() # TENSOR
		#x_lengths = x_lengths.unsqueeze(1)
		#print("x",x.shape)
		#print("x_lengths",x_lengths)
		#print("x_lengths",x_lengths.shape)
		self.packing = x_lengths[0]>0 # vienen secuencias de largo 0 en build

		# loop
		for i in range(self.layers+1):
			#x = self.apply_batchnorm(x, i) # BATCHNORM
			x = self.generate_packing_object(x, x_lengths)
			x, ht = self.stack_rnn[i](x) # RNN
			x, x_lengths = self.generate_tensor_object(x, x_lengths)
			#x = x.transpose(0, 1)
			x = self.dropout(x)
			#print(x.shape)

		if self.rnn_class in ['LSTM']: # LSTM or other special output
			ht = ht[0]

		#print('ht',ht.shape)
		last_ht = ht[-1,:,:] # -1 for last layer
		#if self.bi_lstm:
	    #last_ht = torch.cat([ht[-2,:,:], ht[-1,:,:]],1)
		y_pred = self.last_fc(x)

		if self.packing:
			if not self.training: # in val mode
				#print(output_len[0])
				if self.bi_lstm:
					last_ht = torch.cat([ht[-2,:,:], ht[-1,:,:]],1)
				#print("last_ht",last_ht.shape)
				#print(output[0,:,0])
				#print("ht",last_ht[0,0])

				last_y_pred = self.last_fc(last_ht)
				#print("y_pred",y_pred.shape)
				#print("last_y_pred",last_y_pred.shape)
				#print("y_pred",y_pred[0,:,0])
				#print("last_y_pred",last_y_pred[0,0])
				return y_pred, x_lengths, last_y_pred
			return y_pred, x_lengths # in validation mode
		return y_pred # if not packing
