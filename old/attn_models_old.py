from __future__ import print_function
from __future__ import division

import torch
from torch.autograd import Function
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn_utils
import torch.nn as nn
import math
import numpy as np
from rnn_utils import FinalLinear, get_RNNcell

'''
if self.attn_mode=='correntropy':
# ql = bxhxqxn
# kl = bxhxtxn
ql = ql.unsqueeze(3)#.repeat(1,1,1,3,1) # bxhxqxn -> # bxhxqxtxn
kl = kl.unsqueeze(2)#.repeat(1,1,3,1,1) # bxhxtxn -> # bxhxqxtxn
#print('ql',ql.shape,ql[0,0,:,0])
#print('kl',kl.shape,kl[0,0,:,0])
#std = 10
x = kl-ql

dim = -1
d = kl.shape[-1]
d = 1
N = ql.shape[dim]
eps=1e-4
std_x = x.std(dim=dim)
#print('std_x',std_x.shape)
#print('std_x',std_x[0,:,:])

#fac = (4/N/(2*d+1))**(1/d+4) # very small
#fac = 1
fac = 1.0592*N**(-1/5)

std = std_x*fac+eps
std = std.unsqueeze(-1)
#print('std',std.shape,std[0,:,:])
#print('x',x.shape,x[0,0,:,:])
scores = torch.mean(torch.exp(-x.pow(2)/(2*std.pow(2))),dim=-1) #  = bxhxtxq
#scores = scores/ql.shape[-1]
#print('scores',scores.shape)

'''

#################################################################################################3
#################################################################################################3

#ATTN_TYPE = 'cos' # default is cosine - more stable??
ATTN_TYPE = 'sdot'

#ATTN_FUN = 'sigmoid' # SIGMOID # da pesimo
ATTN_FUN = 'softmax' # SOFTMAX

#ATTN_LINEAR = 'PLinearSimple'
ATTN_LINEAR = 'PLinear'

def subsequent_mask(size):
	"Mask out subsequent positions."
	attn_shape = (1, size, size)
	subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
	return torch.from_numpy(subsequent_mask) == 0

#######################################################################################33

class PLinearSimple(nn.Module):
	def __init__(self, input_features, attention_features, dropout):
		super(PLinearSimple, self).__init__()
		self.dropout = nn.Dropout(dropout)
		self.fc1 = nn.Linear(input_features, attention_features)

	def forward(self, x):
		x = self.fc1(self.dropout(x))
		return x

class PLinear(nn.Module):
	def __init__(self, input_features, attention_features, dropout):
		super(PLinear, self).__init__()
		self.dropout = nn.Dropout(dropout)
		self.fc1 = nn.Linear(input_features, input_features)
		self.fc2 = nn.Linear(input_features, attention_features)

	def forward(self, x):
		x = F.relu(self.fc1(self.dropout(x)))
		x = self.fc2(self.dropout(x))
		#x = self.fc2(x) # USAR DROPOUT O NO?
		#x = F.tanh(self.fc2(self.dropout(x)))
		return x

def get_PLinear(class_name):
	if class_name=='PLinear':
		return PLinear
	if class_name=='PLinearSimple':
		return PLinearSimple
	raise Exception('No class with this name: '+class_name)

#######################################################################################33

class MultiHeadAttentionContext(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, scores_dropout=0.0, ignored_steps_indices=None):
		super(MultiHeadAttentionContext, self).__init__()
		self.input_features = input_features
		self.attention_features = head_size*dim_per_head
		print('attention_features:{} head_size:{} dim_per_head:{}'.format(self.attention_features,head_size,dim_per_head))
		self.head_size = head_size
		self.output_features = self.attention_features
		
		dropout = 0.2
		#self.v_linear = PreLinear_basic(self.input_features, self.attention_features, dropout)
		PreLinear_class = get_PLinear(ATTN_LINEAR)
		self.v_linear = PreLinear_class(self.input_features, self.attention_features, dropout)
		self.k_linear = PreLinear_class(self.input_features, self.attention_features, dropout)
		self.q_linear = PreLinear_class(self.input_features, self.attention_features, dropout)

		self.scores_dropout = nn.Dropout(scores_dropout) # WHAT
		self.fast_mode = True # OJO con esto
		self.attn_mode = ATTN_TYPE
		print('attn_mode:',self.attn_mode)

	def get_output_dims(self):
		return self.output_features

	def forward(self, input):
		return self.forward_mh(input, input, input)

	def forward_mh(self, queries, keys, values):
		b = values.shape[0]
		t = values.shape[1]
		q_size = queries.shape[1]
		h = self.head_size
		#print('queries',queries.shape);print('keys',keys.shape);print('values',values.shape)
		debug = 0

		if not debug:
			queries = self.q_linear(queries) # queries = bxqxn
			keys = self.k_linear(keys) # keys = bxtxn
			values = self.v_linear(values) # values = bxtxn
			pass
		#print('queries',queries.shape);print('keys',keys.shape);print('values',values.shape)

		#queries = self.tanh(queries);keys = self.tanh(keys);values = self.tanh(values) # TANH - no es tan util en realidad... mp sirve para mayor dim, ni para multiHead

		#.view(b,h,q_size,-1) # bxhxqxn
		#ql = queries.view(b,h,-1,q_size) # bxhxqxn
		#print('ql',ql.shape)

		#kl = self.k_linear(keys).view(b,h,-1,t) # bxhxnxt
		#kl = keys.view(b,h,-1,t) # bxhxtxn
		vl = values.view(b,h,t,-1) # bxhxtxn

		#print('ql',ql[0,0,:,:])
		#print('kl',kl[0,0,:,:])

		if (not self.fast_mode): # SLOW mode # deberia hacer algo???
			ql = queries.view(b,h,-1,q_size) # bxhxnxq
			kl = keys.view(b,h,t,-1) # bxhxtxn
			
			#scores = torch.matmul(ql, kl) # bxhxqx1xn * bxhxqxnxt = bxhxqxt
			scores = torch.matmul(kl,ql) # bxhxtxn * bxhxnxq = bxhxtxq
			#print(scores.max(),scores.min())
			#print(scores.min())

		else: # FAST mode
			ql = queries.view(b,h,q_size,-1) # bxhxqxn
			kl = keys.view(b,h,t,-1) # bxhxtxn
			#print('kl',kl[0,0,:,:])
			#print('ql',ql[0,0,:,:])

			
			#print(ql.min(),ql.max())

			inf_value = 1e21

			if self.attn_mode=='cos':
				ql = torch.transpose(ql,-1,-2) # bxhxqxn -> bxhxnxq
				norm_kl = F.normalize(kl, p=2, dim=-1)
				norm_ql = F.normalize(ql, p=2, dim=-2)
				#print('norm_kl',torch.norm(norm_kl,p=2, dim=-1))
				#print('norm_ql',torch.norm(norm_ql,p=2, dim=-2))
				scores = torch.matmul(norm_kl,norm_ql) # bxhxtxn * bxhxnxq = bxhxtxq
				#inf_value = 1e9

			if self.attn_mode=='dot':
				ql = torch.transpose(ql,-1,-2) # bxhxqxn -> bxhxnxq
				scores = torch.matmul(kl,ql) # bxhxtxn * bxhxnxq = bxhxtxq 

			if self.attn_mode=='sdot':
				ql = torch.transpose(ql,-1,-2) # bxhxqxn -> bxhxnxq
				scores = torch.matmul(kl,ql)/math.sqrt(kl.shape[-1]) # bxhxtxn * bxhxnxq = bxhxtxq 
				#scores = torch.clamp(scores, min=-10, max=10)
			
			#inf_value = float('-inf')
			#print('inf_value',inf_value)

			#ind1, ind2 = torch.triu_indices(t,t,offset=0) # kill self attention # se rompe todo :(, no se por que, da como 94% altiro en mean... ya se por que pasa
			#ind1, ind2 = torch.triu_indices(t,t,offset=1)

			
			mask = subsequent_mask(scores.shape[-2])
			#print('mask',mask.shape,mask)
			scores = scores.masked_fill(torch.as_tensor(mask).to(scores.device)==0, -inf_value)

			'''
			ignored_steps_indices = [0,1,2,5,6]
			if not ignored_steps_indices is None:
				scores[:,:,:,ignored_steps_indices] = -inf_value

			first_consecutive_ignored = []
			last_i = -1
			for i in ignored_steps_indices: # ignored_steps_indices MUST BE SORTED
				if not i-last_i==1:
					break
				first_consecutive_ignored.append(i)
				last_i = i
			'''

		#print('q_linear',self.q_linear.weight.max().data.item(),'k_linear',self.k_linear.weight.max().data.item(),'v_linear',self.v_linear.weight.max().data.item()) # no veo problemas
		#print('q_linear',self.q_linear.weight.min().data.item(),'k_linear',self.k_linear.weight.min().data.item(),'v_linear',self.v_linear.weight.min().data.item()) # no veo problemas

		#print('scores',scores.shape,scores[0,0,:10,:10])
		scores = scores.view(b,h,-1,t) # bxhxtxq -> bxhxqxt
		#scores = torch.transpose(scores,-1,-2) # bxhxtxq -> bxhxqxt

		#soft = scores

		if ATTN_FUN=='sigmoid':
			soft = F.sigmoid(scores) # SIGMOID bxhxqxt
		elif ATTN_FUN=='softmax':
			soft = F.softmax(scores, dim=-1) # SOFTMAX bxhxqxt
		else:
			raise Exception('no attn fun')
		
		soft = self.scores_dropout(soft) # SCORES DROPOUT - WHAT?
		#print('soft',soft.shape,soft[0,0,:10,:10])

		output = torch.matmul(soft, vl) # bxhxqxt * bxhxtxn = bxhxqxn
		output = output.contiguous().view(b,q_size,-1) # bxhxqxn -> bxqx(hxn) # <- CAT!!
		#print('output',output.shape,output.max().data.item(),output.min().data.item()) # CRECE!!! SUPER UTIL

		#output = self.tanh(output)
		return output, soft # bxqx(hxn) , bxhxqxt

#################################################################################################3
#################################################################################################3

class LayerNorm(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, features, eps=1e-4):
		super(LayerNorm, self).__init__()
		self.a_2 = nn.Parameter(torch.ones(features))
		self.b_2 = nn.Parameter(torch.zeros(features))
		self.eps = eps

	def forward(self, x, lens):
		# METER PARCHE DE IGNORAR SI SON TODOS 0?
		#print('x',x.shape)
		#print('x',x[:3,-20:,0])
		#print('lens',lens[:3])
		mean = x.mean(-1, keepdim=True)
		std = x.std(-1, keepdim=True)
		norm = self.a_2 * (x - mean) / (std + self.eps) + self.b_2
		return norm

	def forward___(self, x, lens):
		# METER PARCHE DE IGNORAR SI SON TODOS 0?
		print('x',x.shape)
		print('x',x[:3,-20:,0])
		print('lens',lens[:3])
		mean = x.mean(-1, keepdim=True)
		std = x.std(-1, keepdim=True)
		norm = self.a_2 * (x - mean) / (std + self.eps) + self.b_2
		return norm

class MultiHeadAttnCell(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, output_features=None, dropout=0, use_cat=False):
		super(MultiHeadAttnCell, self).__init__()

		self.input_features = input_features
		self.attention_features = head_size*dim_per_head
		self.head_size = head_size
		self.attn = MultiHeadAttentionContext(input_features, head_size, dim_per_head)
		
		#self.add_final_fc = True
		self.add_final_fc = False
		print('add_final_fc:',self.add_final_fc)

		self.output_features = output_features
		if self.output_features is None: # output_dim is NOT defined
			print('-> self.output_features is None!!')
			self.output_features = self.attention_features
		print('output_features:',output_features)


		self.use_cat = use_cat # cat input with output from attn
		print('use_cat:',self.use_cat)
		if self.use_cat:
			self.output_features = self.attention_features+self.input_features

		if self.add_final_fc: # arreglar??
			self.dropout = nn.Dropout(dropout)
			if self.use_cat:
				fc1_in = self.attention_features+self.input_features
			else:
				fc1_in = self.attention_features
			#self.fc1 = nn.Linear(fc1_in, fc1_in) # BIG
			self.fc1 = nn.Linear(fc1_in, self.output_features) # SMALL
			self.fc2 = nn.Linear(self.output_features, self.output_features)
			self.cat_norm = LayerNorm(fc1_in)

		self.input_norm = LayerNorm(self.input_features)
		self.use_batchnorm = False # batchnorm in input and pre-cat to finalFC
		print('use_batchnorm:',self.use_batchnorm)
		
	def get_output_dims(self):
		return self.output_features

	def fully_connected_forward(self, input):
		input = F.tanh(self.fc1(self.dropout(input))) # just because last one was a tanh
		input = self.fc2(self.dropout(input))
		return input

	def forward(self, input, lens):
		if self.use_batchnorm:
			input = self.input_norm(input, lens)
		output_att, scores = self.attn(input)
		
		#if self.use_residual:
			#output1 = input + output1 # RESIDUAL
		#	pass

		if self.use_cat:
			output_att = torch.cat([output_att,input],dim=-1)

		if self.add_final_fc:
			if self.use_batchnorm:
				#output_att = self.cat_norm(output_att, lens)
				pass

			output = self.fully_connected_forward(output_att) # FC

		#if self.use_residual:
			#output2 = output1 + output2 # RESIDUAL
		#	pass
		return output_att, scores


#################################################################################################3
#################################################################################################3

class AttentionOverPast(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, output_features=None, dropout=0, use_cat=False):
		super(AttentionOverPast, self).__init__()
		self.input_features = input_features
		self.head_size = head_size
		self.dim_per_head = dim_per_head
		#self.dim_per_head = 64 # OJOOOOOOOOOOOOOOOOOOOOOOOOOO

		self.attentionContext = MultiHeadAttnCell(self.input_features, self.head_size, self.dim_per_head, output_features, dropout, use_cat=use_cat)
		self.output_features = self.attentionContext.output_features
		self.fast_mode = True

	def get_output_dims(self):
		return self.output_features

	def forward(self, input, lens): # bxtxn n
		if self.fast_mode:
			x_, x_last, s, s_last = self.forward_fast(input, lens) # bxtxn n 
		else:
			x_, x_last, s, s_last = self.forward_slow(input, lens) # bxtxn n

		return x_, x_last, s, s_last

	def forward_test(self, input, lens): # bxtxn n
		aux=input.clone()
		aux2=input.clone()
		aux_lens=lens.clone()

		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[2,3,54,7,9,0,0,7,6,7,11],[7,8,9,6,0,0,0,0,0,0,0],[4,7,79,54,5,2,6,0,5,0,0]],device=input.device).unsqueeze(-1)
		#aux_lens = torch.tensor([6,11,4,9],device=input.device)
		
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,1)
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,2)
		#aux_lens = torch.tensor([7],device=input.device)

		#print(aux.shape);print(aux[:,:,0]);print(aux[:,:,1])
		x_, x_last, s_last = self.forward_slow(aux, aux_lens) # bxtxn n
		print('='*60 + 'SLOW')
		print(x_[0,:,0])
		print(x_last[0,0])
		print(s_last[0,0,:])
		print('='*60)

		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[2,3,54,7,9,0,0,7,6,7,11],[7,8,9,6,0,0,0,0,0,0,0],[4,7,79,54,5,2,6,0,5,0,0]],device=input.device).unsqueeze(-1)
		#aux_lens = torch.tensor([6,11,4,9],device=input.device)
		
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,1)
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,2)
		#aux_lens = torch.tensor([7],device=input.device)

		#print(aux.shape);print(aux[:,:,0]);print(aux[:,:,1])
		x_, x_last, s_last = self.forward_fast(aux2, aux_lens) # bxtxn n
		print('='*60 + 'FAST')
		print(x_[0,:,0])
		print(x_last[0,0])
		print(s_last[0,0,:])
		print('='*60)
		return x_, x_last, s_last

	def forward_fast(self, input, lens): # bxtxn n

		b = input.shape[0]
		t = input.shape[1]
		input = rnn_utils.pack_padded_sequence(input, lens, batch_first=True, enforce_sorted=False) # to padd zero!
		input, lens = nn.utils.rnn.pad_packed_sequence(input, batch_first=True, total_length=t) # RECIVE SEQUENCE
		
		#query = input
		#keys = input
		#values = input
		x_, s_last_ = self.attentionContext(input, lens) # bxqx(hxn) , bxhxqxt
		#print('x_',x_.shape);print('s_last_',s_last_.shape)
		#print(s_last_[0,0,:,:])
		s_seq = torch.transpose(s_last_,1,2) # bxqxhxt
		#print('s_seq',s_seq[0,:,0,:])

		x_ = rnn_utils.pack_padded_sequence(x_, lens, batch_first=True, enforce_sorted=False) # to padd zero!
		x_, lens = nn.utils.rnn.pad_packed_sequence(x_, batch_first=True, total_length=t) # RECIVE SEQUENCE

		#print(x_.shape)
		#print(x_[0,:,0])

		#################################

		s_last = torch.zeros((b,self.head_size,t))

		x_seq_as_q = rnn_utils.pack_padded_sequence(x_, lens, batch_first=True, enforce_sorted=False)
		s_seq_as_q = rnn_utils.pack_padded_sequence(s_seq, lens, batch_first=True, enforce_sorted=False)

		x_seq_as_q, batch_sizes, sorted_indices, unsorted_indices = x_seq_as_q
		s_seq_as_q, batch_sizes, sorted_indices, unsorted_indices = s_seq_as_q

		max_batch_size = int(batch_sizes[0])
		x_last = torch.zeros((max_batch_size,x_.shape[-1]),device=input.device) # error a futuro?

		batch_size_ = 0
		for k, batch_size in enumerate(batch_sizes):
			xx = x_seq_as_q[batch_size_:batch_size_+batch_size]
			x_last[:batch_size,:] = xx

			ss = s_seq_as_q[batch_size_:batch_size_+batch_size]
			#print(ss.shape)
			s_last[:batch_size,:] = ss

			batch_size_ += batch_size

		x_last = x_last[unsorted_indices]
		s_last = s_last[unsorted_indices]
		#print(x_last[0,0])
		return x_, x_last, s_last_, s_last # is better to return the index? for dropout?

	def forward_slow(self, input, lens): # bxtxn n
		b = input.shape[0]
		t = input.shape[1]
		x = torch.zeros((b,t,self.output_features),device=input.device)
		s_last = torch.zeros((b,self.head_size,t))
		old_input = torch.zeros_like(input)
		input = rnn_utils.pack_padded_sequence(input, lens, batch_first=True, enforce_sorted=False)
		input, batch_sizes, sorted_indices, unsorted_indices = input
		max_batch_size = int(batch_sizes[0])
		x_last = torch.zeros((max_batch_size,x.shape[-1]),device=input.device)
		#print('x',x.shape);print('x_last',x_last.shape);print('s_last',s_last.shape)
		batch_size_ = 0
		for k, batch_size in enumerate(batch_sizes):
			#print('batch_size:',batch_size)
			query = input[batch_size_:batch_size_+batch_size]
			batch_size_ += batch_size
			#print('query',query[:,0])
			
			old_input[:batch_size,k,...] = query
			values = old_input[:batch_size,:k+1,:] # k+1
			keys = values

			#print('query',query.shape);print('values',values.shape)
			#print('values',values)
			
			x_, s_last_ = self.attentionContext(query.unsqueeze(1), keys, values)
			#print('x_',x_.shape);print('x_last_',x_last_.shape);print('s_last_',s_last_.shape)
			x[:batch_size,k,...] = x_[:,0,:] # bxqx(hxn)
			x_last[:batch_size] = x_[:,0,:]
			s_last[:batch_size,:,:k+1] = s_last_[:,:,0,:] # bxhxqxt
			
		x = x[unsorted_indices] # just a new order
		x_last = x_last[unsorted_indices] # just a new order
		s_last = s_last[unsorted_indices] # just a new order
		# usar pad_packed_sequence ??
		return x, x_last, s_last # is better to return the index? for dropout?

#################################################################################################3
#################################################################################################3

class RecursiveFeatures_AttentionOverPast(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, rnn_cell, dropout=0, layers=1, use_cat=False):
		super(RecursiveFeatures_AttentionOverPast, self).__init__()
		self.input_features = input_features
		self.dim_per_head = dim_per_head

		self.rnn = rnn_cell(self.input_features, self.dim_per_head, num_layers=layers, batch_first=True, dropout=dropout)

		transformer_like = 1 # when using multihead is D/H
		if transformer_like:
			output_features = dim_per_head
			self.attOP = AttentionOverPast(self.dim_per_head, head_size, dim_per_head//head_size, output_features=output_features, dropout=dropout, use_cat=use_cat)
		else:
			output_features = head_size*dim_per_head
			self.attOP = AttentionOverPast(self.dim_per_head, head_size, dim_per_head, output_features=output_features, dropout=dropout, use_cat=use_cat)
		
		self.output_features = self.attOP.output_features
		self.dropout = nn.Dropout(dropout)

	def get_output_dims(self):
		return self.output_features

	def forward(self, x, x_lengths): # txn n
		#print('x',x.shape)

		# RNN
		x_gru = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # RECIVE TENSORES
		x_gru, last_x_gru = self.rnn(x_gru)
		#last_x_gru = last_x_gru[-1,:,:]
		x_gru,_ = nn.utils.rnn.pad_packed_sequence(x_gru, batch_first=True,total_length=x.shape[1]) # RECIVE SEQUENCE
		x_gru = self.dropout(x_gru)

		# ATT
		x_att, last_x_att, s, last_s = self.attOP(x_gru, x_lengths)
		#print('x_att',x_att.shape);print('x_gru',x_gru.shape)
		#print('last_x_att',last_x_att.shape);print('hn',hn.shape)
		
		x = x_att
		x_last = last_x_att
		return x, x_last, s, last_s

class AttentionModel(nn.Module):
	def __init__(self, model_parameters):
		super(AttentionModel, self).__init__()

		self.model_parameters = model_parameters
		input_dim = model_parameters['input_dim']-1 # last is length
		latent_dim = model_parameters['latent_dim']
		output_dim = model_parameters['n_classes']
		rnn_layers = model_parameters['latent_layers']
		dropout = model_parameters['dropout']
		rnn_cell = get_RNNcell(model_parameters['rnn_class'])
		self.use_cat = model_parameters['attn_use_cat']
		self.noisy = model_parameters['noisy']

		#self.mode = 'AttOP' # 'AttentionOverPast'
		self.mode = 'AttnOP' # 'RecursiveFeatures_AttentionOverPast'
		print('attn mode:',self.mode)
		self.attention = nn.ModuleList()

		if self.mode == 'AttnOP':
			attn_heads = model_parameters['attn_heads']
			units = model_parameters['attn_dim']

			head_size = attn_heads[0]
			(head_size, dim_per_head) = (head_size, units) # (4, units) # 6, 15
			attention = RecursiveFeatures_AttentionOverPast(input_dim, head_size, dim_per_head, rnn_cell, dropout=dropout, layers=rnn_layers, use_cat=self.use_cat)
			attention_dim = attention.get_output_dims()
			self.attention.append(attention)
			print('attention_dim:',attention_dim)

			for head_size in attn_heads[1:]:
				print('-'*60)
				(head_size, dim_per_head) = (head_size, units) # (4, units) # 6, 15
				attention = AttentionOverPast(attention_dim, head_size, dim_per_head, dropout=dropout, output_features=dim_per_head, use_cat=self.use_cat)
				attention_dim = attention.get_output_dims()
				self.attention.append(attention)
				print('attention_dim:',attention_dim)
		
		self.dropout = nn.Dropout(dropout)
		self.last_fc = FinalLinear(attention_dim, output_dim, dropout)
		self.get_name()
		#self.normal = torch.distributions.Normal(torch.tensor([0,0]), torch.tensor([1,1]))

	def print(self):
		print('model_parameters:',self.model_parameters)

	def get_name(self):
		mp = self.model_parameters
		name = mp['rnn_class']+'_'+self.mode
		name += '_'+"".join(mp['channel_names'])
		name += '_dout{}'.format(int(mp['dropout']*100))
		name += '_L{}'.format(mp['latent_layers'])
		name +='_U{}'.format(mp['latent_dim'])
		
		name += '-Attn'
		name += '_U{}'.format(mp['attn_dim'])
		name += '_H{}'.format(mp['attn_heads'])
		name += '_{}'.format(ATTN_LINEAR)
		name +='_{}'.format(ATTN_TYPE)

		if ATTN_FUN=='sigmoid':
			name +='_{}'.format(ATTN_FUN)
		if self.use_cat:
			name +='_{}'.format('cat')
		if self.noisy:
			name +='_{}'.format('noisy')

		self.name = name

	def add_noise_old(self, x):
		if self.noisy and self.training:
				# A LA MALA
				for b in range(x.shape[0]):
					for t in range(x.shape[1]):
						#print('sigma',x[0,:,-1])
						sigma = x[b,t,-1]
						old_mag = x[b,t,-2]
						#noise = torch.rand(old_mag-sigma,old_mag+sigma)
						noise = torch.FloatTensor(1, 1).uniform_(old_mag-sigma, old_mag+sigma)
						#print('old_mag',old_mag,'sigma',sigma,'noise',noise)
						x[b,t,-2] = noise
				#return x+noise
		return x

	def add_noise(self, x):
		if self.noisy and self.training:
			mag_index = 1
			sigma = x[:,:,-1]
			old_mag = x[:,:,mag_index]
			noise = torch.randn(sigma.shape).to(sigma.device)
			x[:,:,mag_index] = old_mag+noise*(sigma**2) # da mejor en ZTF
			#x[:,:,mag_index] = old_mag+noise*sigma
			#print(noise)
		return x

	def forward(self, x_and_lengths):
		x_and_lengths = x_and_lengths.float()
		x = x_and_lengths[:,:,:-1]
		x = self.add_noise(x)

		#x_lengths = x_and_lengths[:,0,-1].cpu().numpy() # NUMPY
		x_lengths = x_and_lengths[:,0,-1].long() # TENSOR
		#x_lengths = x_lengths.unsqueeze(1)
		#print("x",x.shape)
		#print("x_lengths",x_lengths)
		#print("x_lengths",x_lengths.shape)
		self.packing = x_lengths[0]>0 # vienen secuencias de largo 0 en build
		
		last_s = [] # podria tener problemas con la memoria en train
		for i in range(0,len(self.attention)):
			#print('in',i,x.shape)
			x, last_x_att, s, last_s_ = self.attention[i](x, x_lengths)
			last_s.append(last_s_)
			x = self.dropout(x)
			last_x_att = self.dropout(last_x_att) # use this?
			#print('out',i,x.shape)

		#print('x',x.shape);print('x',x[0,:,0])
		#print('last_x_att',last_x_att.shape);print('last_x_att',last_x_att[0,0])
		#print('last_s',last_s[0,0,:]) # head: 0
		
		#x, last_ht = self.attention2(x)

		y_pred = self.last_fc(x)
		x_lengths = x_lengths.to('cpu')
		#print(y_pred.shape)
		
		if self.packing:
			if not self.training: # in val mode
				last_y_pred = self.last_fc(last_x_att)
				#print("y_pred",y_pred.shape)
				#print("last_y_pred",last_y_pred.shape)
				#print("y_pred",y_pred[0,:,0])
				#print("last_y_pred",last_y_pred[0,0])
				return y_pred, x_lengths, last_y_pred, last_s, s
			
			return y_pred, x_lengths, s
		return y_pred # if not packing

#################################################################################################3
#################################################################################################3
