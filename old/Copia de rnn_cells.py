from __future__ import print_function
from __future__ import division

import torch
from torch.autograd import Function
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn_utils
import torch.nn as nn
from batch_normalization_LSTM import BNLSTMCell, LSTM

class RNN(torch.nn.RNN): # auxiliar for normal RNN but with relu
	def __init__(self, *args, **kwargs):
		nonlinearity = 'relu'
		kwargs['nonlinearity'] = nonlinearity
		#torch.nn.RNN.__init__(*args, **kwargs)
		super(RNN, self).__init__(*args, **kwargs)
		
def phi(times, s, tau):
	
	tt=times - s
	x = torch.div(tt,tau)
	x=torch.floor(x)
	x2=tau*x
	x3=tt-x2
	return torch.div(x3, tau)

def time_gate_fast(phase, r_on):
	
	leak_rate=0.001
	training_phase=True
	if not training_phase:
		leak_rate = 0.0

	cond1=torch.where(torch.le(phase, 0.5 * r_on) ,  2.0 * phase / r_on, torch.zeros_like(phase))
	cond2= torch.where((torch.gt(phase, 0.5 * r_on) ) & (torch.lt(phase,r_on) ),  2.0 - 2.0 * phase / r_on, torch.zeros_like(phase))   
	cond3= torch.where(torch.ge(phase,r_on) , leak_rate * phase, torch.zeros_like(phase))

	term=cond1+cond2+cond3
	return term

def phi2(times, s, tau):
	
	tt=times - s
	x = torch.div(tt,tau)
	x=torch.floor(x)
	x2=tau*x
	x3=tt-x2
	return torch.div(x3, tau)

def time_gate_fast2(phase, r_on):
	
	leak_rate=0.001
	training_phase=True
	if not training_phase:
		leak_rate = 0.0

	cond1=torch.where(torch.le(phase, 0.5 * r_on) ,  2.0 * phase / r_on, torch.zeros_like(phase))
	cond2= torch.where((torch.gt(phase, 0.5 * r_on) ) & (torch.lt(phase,r_on) ),  2.0 - 2.0 * phase / r_on, torch.zeros_like(phase))   
	cond3= torch.where(torch.ge(phase,r_on) , leak_rate * phase, torch.zeros_like(phase))

	term=cond1+cond2+cond3
	return term


def get_rnn_cell(cell_name):
	if cell_name=='GRU':
		return torch.nn.GRU
	if cell_name=='LSTM':
		return torch.nn.LSTM
	if cell_name=='RNN':
		return RNN
	if cell_name=='PLSTM':
		return torch.nn.LSTMCell #PLSTM
	raise Exception('No RNN class with this name: {}'.format(cell_name))

class RNN_stack(nn.Module):
	'''
	here the RNN stack is created with the RNN_cell
	'''
	def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi):
		#super(RNN_stack, self).__init__()
		super().__init__()
		print('RNN_cell:',cell_name)
		RNN_cell = get_rnn_cell(cell_name) # get the RNN cell
		hidden_size = output_dim//(1+int(bi))
		self.stack = RNN_cell(input_size=input_features,hidden_size=hidden_size,num_layers=num_layers,dropout=dropout,bidirectional=bool(bi),batch_first=True)

	def forward(self, x, x_lengths):
		#print('x',x.shape)
		pack = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # argument is tensor
		#print('pack',pack)
		x, hidden = self.stack(pack) # (ht, ct)
		#print('x',x)
		x,_ = nn.utils.rnn.pad_packed_sequence(x, batch_first=True) # argument is Sequence

#        print('test')
		if not (isinstance(hidden, list) or isinstance(hidden, tuple)):
			hidden = (hidden, None)

		return x, hidden

class PLSTM_stack(nn.Module):
	'''
	here the PLSTM stack
	'''
	def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi):
		#super(RNN_stack, self).__init__()
		super().__init__()
		self.num_layers=num_layers
	   
		print('RNN_cell:',cell_name) # PSLTM
		RNN_cell = get_rnn_cell(cell_name) # get the RNN cell
		hidden_size = output_dim//(1+int(bi))
		self.input_size=input_features
		self.hidden_size=hidden_size
		self.stack = RNN_cell(input_size=input_features,hidden_size=hidden_size)#,num_layers=num_layers)#,dropout=dropout,bidirectional=bool(bi),batch_first=True)
	   
	def forward(self, x, x_lengths):
		'''

		Parameters
		----------
		x (tensor): shape: (bxtxf)
		x_lengths(tensor): shape: (b) 
		'''
		## FORWARD INIT

#        print(x.shape)
#        pack = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # argument is tensor
		#print('pack',pack)
		
		
		 # ----------   PLSTM  -----------------
#        t = input_[:, 1][-1]  # Now we only accept one id. We have a batch so it's a bit more complex.    
#        
#        tau = nn.Parameter(torch.FloatTensor(self.input_size))
#        torch.exp(torch.nn.init.uniform_(tau, a=0.0, b=self.tau_init))
#
#        r_on = nn.Parameter(torch.FloatTensor(self.input_size))
#        nn.init.constant_(r_on, self.r_on_init)
#
#        s = nn.Parameter(torch.FloatTensor(self.input_size))
#        torch.nn.init.uniform_(s, a=0.0, b=self.tau_init)
#        
#        t2=t.view(-1, 1)
#        times = t2.repeat(1, self.input_size)
#        phase = phi(times, s, tau)
#        kappa = time_gate_fast(phase, r_on, self._leak_rate, self._training_phase)
#        
		
#        output =h_n 
		
		# ---------- --------------------------- 
		
		
		
		
#        b_size = x.shape[0]
#        print(x.shape)
		max_curve_length = x.shape[1]
		bt= x.shape[0]
		
		hx = torch.zeros(1,self.hidden_size)
		cx = torch.zeros(1, self.hidden_size)
		
		hx_all = torch.zeros(bt,self.hidden_size)
		cx_all = torch.zeros(bt, self.hidden_size)
		
		h_pre=torch.zeros_like(hx)
		c_pre=torch.zeros_like(cx)
#        print('hx',hx.shape)
		b_size = x.shape[0]
#        output= []
		output=torch.zeros(bt,max_curve_length,self.hidden_size)
		for b in range(b_size):
#            x_sub = x[b] # <t,f> 
			output1= []
			zr=torch.zeros(1,max_curve_length,self.hidden_size)
			x_sub_len = x_lengths[b]
			
#            print('a',x_sub_len)
			for t in range(x_sub_len):
#            for t in range(max_curve_length):
				
				sub_x = x[b,t,:]
				sub_x2=torch.unsqueeze(sub_x,0)
#                print('sub',sub_x2.shape)
				hx, cx = self.stack(sub_x2,(hx, cx))
		   
				tt = sub_x2[:, 1][-1]  # Now we only accept one id. We have a batch so it's a bit more complex.    
#                tau = nn.Parameter(torch.FloatTensor(bt,max_curve_length )) 
				tau = nn.Parameter(torch.FloatTensor(1,x_sub_len)) 
				torch.exp(torch.nn.init.uniform_(tau, a=0.0, b=6))
		
				r_on = nn.Parameter(torch.FloatTensor(1,x_sub_len))
#                r_on = nn.Parameter(torch.FloatTensor(bt,max_curve_length ))
				nn.init.constant_(r_on, 0.05)
		
#                s = nn.Parameter(torch.FloatTensor(bt,max_curve_length ))
				s = nn.Parameter(torch.FloatTensor(1,x_sub_len))
				torch.nn.init.uniform_(s, a=0.0, b=6)
		#            print('t',tau)
				t2=tt.view(-1, 1)
				time = t2.repeat(1,x_sub_len)
#                time = t2.repeat(bt,max_curve_length)
				times =torch.tensor(time)
				phase = phi(times, s, tau)
				kappa = time_gate_fast(phase,r_on)
				
				h_post=hx
				c_post=cx
#                print(hx.shape,kappa.shape, tau.shape,r_on.shape,s.shape,time.shape)
				h_n=kappa[:,t]* h_post+(1-kappa[:,t])* h_pre
				c_n=kappa[:,t]* c_post+(1-kappa[:,t])* c_pre
#                c_n=torch.unsqueeze(kappa[t],1)* c_post+(1-torch.unsqueeze(kappa[t],1))* c_pre
#                print(h_n.shape,kappa[:,t].shape)
				h_pre=h_post
				c_pre=c_post
	#                
				hx=h_n
				cx=c_n
				output1.append(torch.unsqueeze(hx,1))
				
			out1= torch.cat(output1, dim=1) 
#            print(torch.cat(output, dim=1).shape )
#           x print(out1.shape )
			zr[:,:t,:]=out1[:,:t,:]
#            print(zr.shape )
			output[b,:,:]=zr
			hx_all[b,:]=hx
			cx_all[b,:]=cx
#            out3=torch.cat((zr,out3),dim=0) 
#        x=torch.cat(zr,dim=0)    
		x=output
#        print(x.shape)
	#        x=hx[-1]
	#        b_size = x.shape[0]
	#        for b in range(b_size):
	#            x_sub = x[b] # <t,f> 
	#            x_sub_len = x_lengths[b]
	#            print('test_1')
	#            print(x_sub_len)
	#            for curve_step in range(x_sub_len):
	##                    out, (h,c) = lstm(x_sub[curve_step]) # <f,>       
	#                hx, cx = self.stack(x_sub[curve_step])
	#            output.append(hx)
		hidden= (torch.unsqueeze(hx,0),torch.unsqueeze(cx,0))
		hidden1= (torch.unsqueeze(hx_all,0),torch.unsqueeze(cx_all,0))
#        print(hidden[0].shape)
		print(x.shape)
		return x, hidden1
		
#class PLSTM2_stack(nn.Module):
#    '''
#    here the PLSTM stack
#    '''
#    def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi):
#        #super(RNN_stack, self).__init__()
#        super().__init__()
#        print('RNN_cell:',cell_name) # PSLTM
#        RNN_cell = get_rnn_cell('LSTM') # get the RNN cell
#        hidden_size = output_dim//(1+int(bi))
#        self.stack = RNN_cell(input_size=input_features,hidden_size=hidden_size,num_layers=num_layers,dropout=dropout,bidirectional=bool(bi),batch_first=True)
#
#    def forward(self, x, x_lengths):
#        '''
#
#        Parameters
#        ----------
#        x (tensor): shape: (bxtxf)
#        x_lengths(tensor): shape: (b) 
#        '''
#        ## FORWARD INIT
#
#        #print('x',x.shape)
#        pack = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # argument is tensor
#        #print('pack',pack)
#        x, hidden = self.stack(pack) # (ht, ct)
#        #print('x',x)
#        x,_ = nn.utils.rnn.pad_packed_sequence(x, batch_first=True) # argument is Sequence
#
#        ## FORWARD END
#
#        if not (isinstance(hidden, list) or isinstance(hidden, tuple)):
#            hidden = (hidden, None)
#        return x, hidden


def get_rnn_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi, use_batchnorm, max_length):
	if cell_name=='PLSTM':
		return PLSTM_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi)

	if use_batchnorm: # it's an exception because is a custom class
		if cell_name!='LSTM':
			raise Exception('BN is only supported with LSTM, not with',cell_name)
		return BN_LSTM_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi, max_length)

	else:
		return RNN_stack(cell_name, input_dim, latent_dim, layers, dropout, is_bi) # get RNN stack!

#########################################

class BN_LSTM_stack(nn.Module):
	def __init__(self, cell_name, input_features, output_dim, num_layers, dropout, bi, max_length):
		#super(RNN_stack, self).__init__()
		super().__init__()
		if bi:
			raise Exception('not supported')

		self.rnn_stack = nn.ModuleList()
		input_size=input_features
		for i in range(num_layers):
			#print('i',i)
			rnn_cell = LSTM(cell_class=BNLSTMCell, input_size=input_size, hidden_size=output_dim, batch_first=True, max_length=max_length)
			input_size = output_dim
			self.rnn_stack.append(rnn_cell)
		self.dropout = nn.Dropout(dropout)

	def forward(self, x, x_lengths):
		#print('in x',x.shape)
		for k,cell in enumerate(self.rnn_stack):
			x, hidden = cell(x, length=x_lengths) # (ht, ct)
			if k<len(self.rnn_stack)-1:
				#print(k)
				x = self.dropout(x)
		#print('out x',x.shape)

		return x, hidden

#########################################

class FinalLinear(nn.Module):
	def __init__(self, input_features, output_dim, dropout):
		#super(FinalLinear, self).__init__()
		super().__init__()
		self.dropout = nn.Dropout(dropout)
		self.fc1 = nn.Linear(input_features, output_dim)
		self.fc2 = nn.Linear(output_dim, output_dim)
		self.softmax = nn.Softmax(dim=-1) # usar log?

	def forward(self, x):
		x = F.relu(self.fc1(self.dropout(x))) # RELU
		x = self.fc2(self.dropout(x))
		x = self.softmax(x)
		return x