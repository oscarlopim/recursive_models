from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F 
from tinyFlame.models import add_model_info, DummyModule, MLP, SimpleMLP
from sequencial_models_utils import PositionalEncoding, SerialParallelCurveHandler

#####################################################################


class Dummy_Parallel(nn.Module):
	def __init__(self, model_parameters, *args, **kwargs):
		super(Dummy_Parallel, self).__init__()
		self.model_parameters = model_parameters
		self.info = {'grad_norm':{'w':0,'b':0}}
		self.name = 'mld-dummyParallel'
		self.rnn_stack = DummyModule()
		self.input_dim = self.model_parameters['input_dim']
		print('rnn_stack:',self.rnn_stack)

	def get_output_dims(self):
		return self.input_dim

	def forward(self, x, *args, **kwargs):
		x = self.rnn_stack(x)
		return x, None, self.info


class Multiband_Model(nn.Module):
	def __init__(self, model_parameters, parallel_models, serial_model):
		super(Multiband_Model, self).__init__()
		self.model_parameters = model_parameters

		# ATTRIBUTES

		self.features_size = model_parameters['input_dim']
		self.max_curve_length = model_parameters['max_curve_length'] # used for optimization
		self.bands = model_parameters['bands_used']
		self.use_onehot_mlp = model_parameters['onehot_mlp']
		self.is_multiband = len(self.bands)>1
		self.dropout = self.model_parameters['dropout']
		self.uses_parallel_dropout = self.model_parameters['uses_parallel_dropout']
		print('Bands: {} - is_multiband: {}'.format(self.bands, self.is_multiband))

		# PARALLEL

		self.freeze_parallel = model_parameters['freeze_parallel']
		self.parallel_models = nn.ModuleList(parallel_models)
		self.parallel_output_dim = parallel_models[0].get_output_dims()
		#print('parallel_output_dim',self.parallel_output_dim)
		self.parallel_grad_function = torch.enable_grad
		if self.freeze_parallel:
			self.parallel_grad_function = torch.no_grad

		# ENCODING

		self.uses_parallel_encoding = False
		self.uses_serial_encoding = False

		encoding_dropout = self.model_parameters['encoding_dropout']
		self.encoding_features_parallel = self.model_parameters['encoding_features_parallel']
		if self.encoding_features_parallel>0:
			assert self.encoding_features_parallel%2 == 0
			max_period_parallel = self.model_parameters['max_period_parallel'] # serial_days
			self.positionalEncoding_parallel = PositionalEncoding(self.encoding_features_parallel//2, max_period_parallel, self.max_curve_length, dropout=encoding_dropout)
			self.encoding_dropout_parallel = nn.Dropout(0.2)
			print('positionalEncoding_parallel:',self.positionalEncoding_parallel)
			self.uses_parallel_encoding = True

		self.encoding_features_serial = self.model_parameters['encoding_features_serial']
		if self.encoding_features_serial>0:
			assert self.encoding_features_serial%2 == 0
			max_period_serial = self.model_parameters['max_period_serial'] # serial_days
			self.positionalEncoding_serial = PositionalEncoding(self.encoding_features_serial//2, max_period_serial, self.max_curve_length, dropout=encoding_dropout)
			print('positionalEncoding_serial:',self.positionalEncoding_serial)
			self.uses_serial_encoding = True

		# SERIAL

		self.serial_model = serial_model
		self.serial_input_dim = self.serial_model.input_dim

		# PRE-SERIAL

		self.serial_onehot_mlp = DummyModule(None)
		if self.use_onehot_mlp: # self.onehot_mlp
			self.generate_onehot_mlp()
		print('serial_onehot_mlp:', self.serial_onehot_mlp)

		self.parallel_dropout = 0.0
		if self.uses_parallel_dropout:
			self.parallel_dropout = self.dropout
		self.parallel_dropout_f = nn.Dropout(self.parallel_dropout)
		print('parallel_dropout_f:',self.parallel_dropout_f)

		# PARALLEL-SERIAL HANDLER

		bands_count = len(self.bands)
		alive_p = 0.05
		self.spCH = SerialParallelCurveHandler(bands_count, alive_p)

		# NAME

		self.get_name()

	def generate_onehot_mlp(self):
		activation = 'relu'
		last_activation = 'relu'
		mlp_layers = 1
		mlp_in = self.parallel_output_dim+len(self.bands)
		self.serial_onehot_mlp = MLP(mlp_in, self.serial_input_dim, [mlp_in]*mlp_layers,
			activation=activation, dropout=self.dropout, last_activation=last_activation)

	def print(self):
		print('model_parameters:',self.model_parameters)

	def get_name(self):
		name = ''
		name += '{}.'.format(self.parallel_models[0].name)
		if self.is_multiband:
			name += '{}.'.format(self.serial_model.name)

		# MB PARAMETERS
		name += add_model_info('pEnc',self.encoding_features_parallel)[1:]
		name += add_model_info('sEnc',self.encoding_features_serial)
		name += add_model_info('bands',''.join(self.bands))
		self.name = name

	def forward(self, x_onehots):
		#print('x_onehots', x_onehots.device, x_onehots.shape, x_onehots[0,:,0])
		x_onehots = self.spCH.randomCurveAgumentation(x_onehots)
		#print('x_onehots', x_onehots.device, x_onehots.shape, x_onehots[0,:,0])
		parallel_holder_list, parallel_lengths_list, parallel_onehot_list, p2s_mapping_indexs_list, onehot_S = self.spCH.serial_to_parallel(x_onehots)
		#print('onehot_S',onehot_S.shape,torch.transpose(onehot_S[0,:],-2,-1))
		DAY_INDEX = 0 # where is the days!!

		######### PARALLEL PROCESSING
		
		parallel_results = []
		for kb, parallel_model in enumerate(self.parallel_models):
			x_P = parallel_holder_list[kb]
			lengths_P = parallel_lengths_list[kb]
			#print('lengths_P', lengths_P[0])
			onehot_P = parallel_onehot_list[kb]
			#print('onehot_P', onehot_P[0])

			if self.uses_parallel_encoding:
				time_P = x_P[:,:,DAY_INDEX]
				#print('time_P',time_P.shape,time_P[0])
				encoding_P = self.positionalEncoding_parallel(time_P, lengths_P)
				x_P = torch.cat([x_P[:,:,1:], encoding_P], dim=-1)
				#print('x_P',x_P.shape,x_P[0])

			with self.parallel_grad_function():
				other_args_P = []
				x_P, last_x_P, info_P = parallel_model(x_P, lengths_P, other_args_P)
			
			#print('x_p',x_p.shape,x_p[0,0,:])
			if self.is_multiband:
				parallel_results.append(x_P)

			else:
				# RETURN NOW!
				return x_parallel, onehot_P, last_x_p, info
		
		######### SERIAL PROCESSING

		#print('SERIAL PROCESSING')
		x_S, lengths_S = self.spCH.parallel_to_serial(parallel_results, parallel_lengths_list, p2s_mapping_indexs_list)
		#print('x_S',x_S.shape)

		#serial_onehots = onehots_mb.transpose(-2,-1).float()
		#print('serial_onehots',serial_onehots.shape)
		other_args_S = {}
		x_S = self.parallel_dropout_f(x_S)

		x_S = torch.cat([x_S, onehot_S.float()], dim=-1) # ALWAYS CAT
		#print('serial_results',serial_results.shape,serial_results[0,0,:])

		if self.uses_serial_encoding:
			time_S = x_onehots[:,:,DAY_INDEX]
			#print('time_S',time_S.shape,time_S[0])
			encoding_S = self.positionalEncoding_serial(time_S, lengths_S)
			#print('encoding',encoding.shape,encoding[0])
			#raise Exception('test')
			x_S = torch.cat([x_S, encoding_S], dim=-1)
			#print('x_S',x_S.shape)

		#print('serial_results',serial_results.shape,serial_results[0,0,:])
		x_S = self.serial_onehot_mlp(x_S)
		#print('x_S',x_S.shape)

		x_S, last_x_S, info_S = self.serial_model(x_S, lengths_S, other_args_S)
		#print('x_S',x_S.shape)

		#print('onehots_mb',onehots_mb.shape,onehots_mb[0])
		#print('serial_results end',serial_results.shape,serial_results[0,0,:])
		#print('serial_results',serial_results.shape,'onehots_mb',onehots_mb.shape,'serial_last_y',serial_last_y.shape)
		return x_S, onehot_S, last_x_S, info_S

	def capture_grad(self):
		for pm in self.parallel_models:
			pm.capture_grad()

	def capture_grad_end(self):
		for pm in self.parallel_models:
			pm.capture_grad_end()