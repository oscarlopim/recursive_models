from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
from tinyFlame.models import generate_tensor_mask, generate_tensor_mask_last_step, MLP, SimpleMLP, count_parameters
#from attn_multihead import MultiHeadAttnCell
import numpy as np

def generate_square_subsequent_mask(sz):
	mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
	mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
	return mask

class LayerNorm_proxy(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, normalized_shape, eps=1e-5, **kwargs):
		super().__init__()
		self.bnorm = nn.LayerNorm(normalized_shape=normalized_shape, eps=eps)

	def forward(self, x, mask):
		x = x.masked_fill(mask==0, 0)
		#x = x.transpose(-2,-1)
		x = self.bnorm(x)
		#x = x.transpose(-2,-1)
		x = x.masked_fill(mask==0, 0)
		return x

class SelfAttention_layer(nn.Module):
	def __init__(self, input_features, attn_heads, max_curve_length, use_residual=True, dropout=0.0, along_curve_dropout=0.0):
		super().__init__()

		# ATRIBUTES

		self.input_features = input_features
		self.attn_heads = attn_heads
		self.is_dummy = (attn_heads==0)

		if input_features%attn_heads!=0:
			raise Exception('error in attn_heads and attn_dims!!! {}/{}'.format(input_features,attn_heads))
		self.units_per_head = input_features//attn_heads

		self.uses_pytorch_implementation = True
		self.use_residual = use_residual
		self.residual_dropout = dropout
		self.along_curve_dropout = along_curve_dropout
		self.attn_bias = True

		# MODULES

		self.residual_dropout_f = nn.Dropout(self.residual_dropout)
		if self.uses_pytorch_implementation:
			self.attentionContext = nn.MultiheadAttention(self.input_features, self.attn_heads, dropout=self.along_curve_dropout, bias=self.attn_bias)
		else:
			self.attentionContext = MultiHeadAttnCell(self.input_features, attn_heads, attn_units_head, max_curve_length, dropout=self.along_curve_dropout)

		self.src_mask = nn.Parameter(generate_square_subsequent_mask(max_curve_length), requires_grad=False)

		activation = 'relu'
		last_activation = 'linear' # THIS IS THE REAL TRANSFORMER
		#last_activation = 'relu'
		hidden_units = [self.input_features]
		self.context_mlp = MLP(self.input_features, self.input_features, hidden_units, activation=activation, dropout=dropout, last_activation=last_activation)

		self.attn_BN = LayerNorm_proxy(normalized_shape=(self.input_features))
		self.MLP_BN = LayerNorm_proxy(normalized_shape=(self.input_features))

	def extra_repr(self):
		return 'input_features={}, out_features={}, attn_heads={}, units_per_head={}, attn_bias={}, use_residual={}, residual_dropout={}, along_curve_dropout={}'.format(
			self.input_features, self.input_features, self.attn_heads, self.units_per_head, self.attn_bias, self.use_residual, self.residual_dropout, self.along_curve_dropout
		)

	def __repr__(self):
		if self.is_dummy:
			return "DummySelfAttention(attn_heads=0)"
		else:
			txt = 'MultiheadAttention({})'.format(self.extra_repr())
			txt += '\n\t - Parameters:{}'.format(count_parameters(self.attentionContext))
			txt += '\n\t{}'.format(self.context_mlp.__repr__())
			return txt

	def get_output_dims(self):
		if self.is_dummy:
			return self.input_features
		else:
			return self.input_features
			#return self.attentionContext.get_output_dims()

	def forward(self, x, curve_length_mask):
		'''
		(bxtxn)
		(bxtx1)
		(bxtx1)

		not cleaned yet!
		'''
		aligments = None

		if not self.is_dummy:
			#print('src_mask',self.src_mask.shape,self.src_mask)
			#print('curve_length_mask',curve_length_mask.shape,curve_length_mask[0])
			key_padding_mask = ~curve_length_mask[:,:,0]#.transpose(1,0)
			#print('pre x attn',x[0,:,0])

			query = x.transpose(1,0)
			key= query
			value = query
			#print('x',x.shape,'query',query.shape,'key',key.shape,'value',value.shape)
			#print('x',x.shape)
			initial_x = x
			x, aligments = self.attentionContext(query, key, value, key_padding_mask=key_padding_mask, need_weights=True, attn_mask=self.src_mask) # bxqx(hxn) , bxhxqxt
			x = x.transpose(1,0)

			if self.use_residual:
				x = self.residual_dropout_f(x)
				x = initial_x+x
				x = self.attn_BN(x, curve_length_mask)

			#print('after x attn',x[0,:,0])
			#print('x',x.shape,'aligments',aligments.shape)
			#aligments = aligments.mean(dim=1) # OJO
			#print('aligments',aligments.shape,aligments[0,:15,:15])
			
			#aligments_last = aligments*last_step_mask#.unsqueeze(-1) # get last # REVISAR!! USAR PADDING?
			#aligments_last = aligments_last
			#print('aligments_last',aligments_last.sum())

		initial_x = x
		x = self.context_mlp(x)
		if self.use_residual:
			x = self.residual_dropout_f(x)
			x = initial_x+x
			x = self.MLP_BN(x, curve_length_mask)

		#print('x',x.shape,x[5,:,5])
		x = x.masked_fill(~curve_length_mask, 0)
		#print('x',x.shape,x[5,:,5])
		return x, aligments

'''
	def forward_test(self, x, mask): # bxtxn n
		aux=input.clone()
		aux2=input.clone()

		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[2,3,54,7,9,0,0,7,6,7,11],[7,8,9,6,0,0,0,0,0,0,0],[4,7,79,54,5,2,6,0,5,0,0]],device=input.device).unsqueeze(-1)
		#aux_lens = torch.tensor([6,11,4,9],device=input.device)
		
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,1)
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,2)
		#aux_lens = torch.tensor([7],device=input.device)

		#print(aux.shape);print(aux[:,:,0]);print(aux[:,:,1])
		x_, x_last, s_last = self.forward_slow(aux, aux_lens) # bxtxn n
		print('='*60 + 'SLOW')
		print(x_[0,:,0])
		print(x_last[0,0])
		print(s_last[0,0,:])
		print('='*60)

		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[2,3,54,7,9,0,0,7,6,7,11],[7,8,9,6,0,0,0,0,0,0,0],[4,7,79,54,5,2,6,0,5,0,0]],device=input.device).unsqueeze(-1)
		#aux_lens = torch.tensor([6,11,4,9],device=input.device)
		
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,1)
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,2)
		#aux_lens = torch.tensor([7],device=input.device)

		#print(aux.shape);print(aux[:,:,0]);print(aux[:,:,1])
		x_, x_last, s_last = self.forward_fast(aux2, aux_lens) # bxtxn n
		print('='*60 + 'FAST')
		print(x_[0,:,0])
		print(x_last[0,0])
		print(s_last[0,0,:])
		print('='*60)
		return x_, x_last, s_last

'''