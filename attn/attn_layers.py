from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.utils.rnn as rnn_utils
from multihead_attn import MultiHeadAttnCell
from tinyFlame.models import generate_tensor_mask, generate_tensor_mask_last_step

class AttentionOverPast(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, max_length_time, output_features=None, dropout=0, use_cat=False, use_residual=False):
		super(AttentionOverPast, self).__init__()
		self.input_features = input_features
		self.head_size = head_size
		self.dim_per_head = dim_per_head
		self.max_length_time = max_length_time

		transformer_like = 1 # when using multihead is D/H
		if transformer_like:
			self.dim_per_head = dim_per_head//head_size

		self.attentionContext = MultiHeadAttnCell(self.input_features, self.head_size, self.dim_per_head, max_length_time, output_features, dropout,
			use_cat=use_cat, use_residual=use_residual)
		self.output_features = self.attentionContext.output_features
		#self.fast_mode = True

	def get_output_dims(self):
		return self.output_features

	def forward(self, input, lens): # bxtxn n
		#x_, x_last, s, s_last = self.forward_fast(input, lens) # bxtxn n 
		#x_, x_last, s, s_last = self.forward_slow(input, lens) # bxtxn n
		x_, x_last, s, s_last = self.forward_turbo(input, lens) # bxtxn n
		return x_, x_last, s, s_last

	def forward_test(self, input, lens): # bxtxn n
		aux=input.clone()
		aux2=input.clone()
		aux_lens=lens.clone()

		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[2,3,54,7,9,0,0,7,6,7,11],[7,8,9,6,0,0,0,0,0,0,0],[4,7,79,54,5,2,6,0,5,0,0]],device=input.device).unsqueeze(-1)
		#aux_lens = torch.tensor([6,11,4,9],device=input.device)
		
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,1)
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,2)
		#aux_lens = torch.tensor([7],device=input.device)

		#print(aux.shape);print(aux[:,:,0]);print(aux[:,:,1])
		x_, x_last, s_last = self.forward_slow(aux, aux_lens) # bxtxn n
		print('='*60 + 'SLOW')
		print(x_[0,:,0])
		print(x_last[0,0])
		print(s_last[0,0,:])
		print('='*60)

		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[2,3,54,7,9,0,0,7,6,7,11],[7,8,9,6,0,0,0,0,0,0,0],[4,7,79,54,5,2,6,0,5,0,0]],device=input.device).unsqueeze(-1)
		#aux_lens = torch.tensor([6,11,4,9],device=input.device)
		
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,1)
		#aux = torch.tensor([[1,2,6,7,8.0,9,0,0,0,0,0],[1,2,6,7,8.0,9,0,0,0,0,0]],device=input.device).view(-1,11,2)
		#aux_lens = torch.tensor([7],device=input.device)

		#print(aux.shape);print(aux[:,:,0]);print(aux[:,:,1])
		x_, x_last, s_last = self.forward_fast(aux2, aux_lens) # bxtxn n
		print('='*60 + 'FAST')
		print(x_[0,:,0])
		print(x_last[0,0])
		print(s_last[0,0,:])
		print('='*60)
		return x_, x_last, s_last

	def forward_turbo(self, input, lens): # bxtxn n
		b = input.shape[0]
		t = input.shape[1]
		#print(lens[:2])
		mask = 1

		#mask = generate_tensor_mask(lens, self.max_length_time, input.device)
		#print(mask.shape,mask[:2,:])
		#mask_last_step = generate_tensor_mask_last_step(lens, self.max_length_time, input.device)
		mask_last_step = mask
		#print(mask_last_step.shape,mask_last_step[:2,:])
		#print('-'*60)

		#print('input',input[:2,:,1])
		
		input = input*mask#.unsqueeze(-1) # 0-padding
		
		#input = rnn_utils.pack_padded_sequence(input, lens, batch_first=True, enforce_sorted=False) # to padd zero!
		#input, lens = nn.utils.rnn.pad_packed_sequence(input, batch_first=True, total_length=t) # RECIVE SEQUENCE
		#print('input',input[:2,:,1])

		#query = input
		#keys = input
		#values = input
		x, aligments = self.attentionContext(input, lens) # bxqx(hxn) , bxhxqxt
		#print('x',x.shape)
		#print('x',x[:2,:,1])
		
		x = x*mask#.unsqueeze(-1) # 0-padding
		
		#print('x',x[:2,:,1])

		x_last = x*mask_last_step#.unsqueeze(-1) # get last
		x_last = x_last.sum(dim=1) # get last
		#print('x_last',x_last.shape)
		#print('x_last',x_last[:2])

		#print('aligments',aligments.shape)
		aligments_last = aligments*mask_last_step#.unsqueeze(-1) # get last # REVISAR!! USAR PADDING?
		aligments_last = aligments_last.sum(dim=2) # get last # REVISAR!! USAR PADDING?

		return x, x_last, aligments, aligments_last

	def forward_fast(self, input, lens): # bxtxn n
		b = input.shape[0]
		t = input.shape[1]
		input = rnn_utils.pack_padded_sequence(input, lens, batch_first=True, enforce_sorted=False) # to padd zero!
		input, lens = nn.utils.rnn.pad_packed_sequence(input, batch_first=True, total_length=t) # RECIVE SEQUENCE
		
		#query = input
		#keys = input
		#values = input
		x_, s_last_ = self.attentionContext(input, lens) # bxqx(hxn) , bxhxqxt
		#print('x_',x_.shape);print('s_last_',s_last_.shape)
		#print(s_last_[0,0,:,:])
		s_seq = torch.transpose(s_last_,1,2) # bxqxhxt
		#print('s_seq',s_seq[0,:,0,:])

		x_ = rnn_utils.pack_padded_sequence(x_, lens, batch_first=True, enforce_sorted=False) # to padd zero!
		x_, lens = nn.utils.rnn.pad_packed_sequence(x_, batch_first=True, total_length=t) # RECIVE SEQUENCE

		#print(x_.shape)
		#print(x_[0,:,0])

		################################# OBTAIN ATTENTION MAPS

		s_last = torch.zeros((b,self.head_size,t))

		x_seq_as_q = rnn_utils.pack_padded_sequence(x_, lens, batch_first=True, enforce_sorted=False)
		s_seq_as_q = rnn_utils.pack_padded_sequence(s_seq, lens, batch_first=True, enforce_sorted=False)

		x_seq_as_q, batch_sizes, sorted_indices, unsorted_indices = x_seq_as_q
		s_seq_as_q, batch_sizes, sorted_indices, unsorted_indices = s_seq_as_q

		max_batch_size = int(batch_sizes[0])
		x_last = torch.zeros((max_batch_size,x_.shape[-1]),device=input.device) # error a futuro?

		batch_size_ = 0
		for k, batch_size in enumerate(batch_sizes):
			xx = x_seq_as_q[batch_size_:batch_size_+batch_size]
			x_last[:batch_size,:] = xx

			ss = s_seq_as_q[batch_size_:batch_size_+batch_size]
			#print(ss.shape)
			s_last[:batch_size,:] = ss

			batch_size_ += batch_size

		x_last = x_last[unsorted_indices]
		s_last = s_last[unsorted_indices]
		#print(x_last[0,0])
		return x_, x_last, s_last_, s_last # is better to return the index? for dropout?

	def forward_slow(self, input, lens): # bxtxn n
		b = input.shape[0]
		t = input.shape[1]
		x = torch.zeros((b,t,self.output_features),device=input.device)
		s_last = torch.zeros((b,self.head_size,t))
		old_input = torch.zeros_like(input)
		input = rnn_utils.pack_padded_sequence(input, lens, batch_first=True, enforce_sorted=False)
		input, batch_sizes, sorted_indices, unsorted_indices = input
		max_batch_size = int(batch_sizes[0])
		x_last = torch.zeros((max_batch_size,x.shape[-1]),device=input.device)
		#print('x',x.shape);print('x_last',x_last.shape);print('s_last',s_last.shape)
		batch_size_ = 0
		for k, batch_size in enumerate(batch_sizes):
			#print('batch_size:',batch_size)
			query = input[batch_size_:batch_size_+batch_size]
			batch_size_ += batch_size
			#print('query',query[:,0])
			
			old_input[:batch_size,k,...] = query
			values = old_input[:batch_size,:k+1,:] # k+1
			keys = values

			#print('query',query.shape);print('values',values.shape)
			#print('values',values)
			
			x_, s_last_ = self.attentionContext(query.unsqueeze(1), keys, values)
			#print('x_',x_.shape);print('x_last_',x_last_.shape);print('s_last_',s_last_.shape)
			x[:batch_size,k,...] = x_[:,0,:] # bxqx(hxn)
			x_last[:batch_size] = x_[:,0,:]
			s_last[:batch_size,:,:k+1] = s_last_[:,:,0,:] # bxhxqxt
			
		x = x[unsorted_indices] # just a new order
		x_last = x_last[unsorted_indices] # just a new order
		s_last = s_last[unsorted_indices] # just a new order
		# usar pad_packed_sequence ??
		return x, x_last, s_last # is better to return the index? for dropout?

#################################################################################################3
#################################################################################################3

class RecursiveFeatures_AttentionOverPast(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, max_length_time, rnn_cell, dropout=0, layers=1, use_cat=False):
		super(RecursiveFeatures_AttentionOverPast, self).__init__()
		self.input_features = input_features
		self.dim_per_head = dim_per_head

		self.rnn = rnn_cell(self.input_features, self.dim_per_head, num_layers=layers, batch_first=True, dropout=dropout)

		transformer_like = 1 # when using multihead is D/H
		if transformer_like:
			output_features = dim_per_head
			self.attOP = AttentionOverPast(self.dim_per_head, head_size, dim_per_head//head_size, max_length_time, output_features=output_features, dropout=dropout, use_cat=use_cat)
		else:
			output_features = head_size*dim_per_head
			self.attOP = AttentionOverPast(self.dim_per_head, head_size, dim_per_head, max_length_time, output_features=output_features, dropout=dropout, use_cat=use_cat)
		
		self.output_features = self.attOP.output_features
		self.dropout = nn.Dropout(dropout)

	def get_output_dims(self):
		return self.output_features

	def forward(self, x, x_lengths): # txn n
		#print('x',x.shape)

		# RNN
		x_gru = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False) # RECIVE TENSORES
		x_gru, last_x_gru = self.rnn(x_gru)
		#last_x_gru = last_x_gru[-1,:,:]
		x_gru,_ = nn.utils.rnn.pad_packed_sequence(x_gru, batch_first=True,total_length=x.shape[1]) # RECIVE SEQUENCE
		x_gru = self.dropout(x_gru)

		# ATT
		x_att, last_x_att, s, last_s = self.attOP(x_gru, x_lengths)
		#print('x_att',x_att.shape);print('x_gru',x_gru.shape)
		#print('last_x_att',last_x_att.shape);print('hn',hn.shape)
		
		x = x_att
		x_last = last_x_att
		return x, x_last, s, last_s


#################################################################################################3
#################################################################################################3

