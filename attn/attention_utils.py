from __future__ import print_function
from __future__ import division

import matplotlib.pyplot as plt
import os
import math
from files import load_pickle, create_folders
import numpy as np

def normalize_heads_per_batch(x, head_size, crop_time=True): # bxhxt
	#print('x',x.shape)
	x = np.transpose(x) # txhxb
	for h in range(head_size):
		min_ = np.min(x[:,h,:],axis=0); max_ = np.max(x[:,h,:],axis=0) # per batch and head
		#min_ = np.min(x[:,h,:]); max_ = np.max(x[:,h,:])
		p = (x[:,h,:]-min_)/(max_-min_)
		#print(p.shape)
		x[:,h,:] = p
		if crop_time:
			base_lv = 0.15
			x[:,h,:] = p*(1-base_lv)+base_lv
	x = np.transpose(x) # bxhxt
	return x

def gray_to_viridis(img, lens):
	cmap=plt.get_cmap('viridis')
	#print(img.shape)
	for b in range(img.shape[0]):
		for h in range(img.shape[1]):
			for j in range(img.shape[2]):
				img[b,h,j] *= int(lens[b]>j)
	#img = (img-img.min())/(img.max()-img.min())
	return np.array(cmap(img))

	
def get_attention_information(load_folder, model_name, attention_layer):
	res = load_pickle('{}/{}.accucm'.format(load_folder, model_name))
	s_per_time = res.results['scores']
	lengths_per_time = res.results['lengths']

	head_size = s_per_time[0][attention_layer][0].shape[1]
	max_length = s_per_time[0][0][0].shape[2] # time # layer # b # tensor
	return s_per_time, lengths_per_time, head_size, max_length


def plot_attention_maps(myModel, load_folder, save_folder, attention_layer, step_index, y_test, class_names, target_head=0, mode='one', crop_time=True,  save_image=False):
	model = myModel.model
	model_name = myModel.model_name
	s_per_time, lengths_per_time, head_size, max_length = get_attention_information(load_folder, model_name, attention_layer)
	print('Model:', model_name)
	print('mode:', mode)
	print('attention_layer:', attention_layer)
	print('head_size:', head_size)
	print('step_index:',step_index)

	step_s_list = s_per_time[step_index]
	output_len_list = lengths_per_time[step_index]

	full_s = np.empty((0,head_size,max_length))
	full_output_len = np.empty((0))
	#print('attention_layer',attention_layer)
	#print('full_s', full_s.shape)
	for k in range(len(step_s_list[attention_layer])): # is a list
		#print(step_s_list[attention_layer][k][:,:head_size,:].shape)
		full_s = np.append(full_s, step_s_list[attention_layer][k][:,:head_size,:], axis=0) # slow, concatenate
		full_output_len = np.append(full_output_len, output_len_list[k], axis=0) # slow, concatenate
		if k>1: # just some of them
			break
	
	rows = 2
	cols = math.ceil(len(class_names)/rows)
	fig, axs = plt.subplots(rows,cols,figsize=(16,14))
	
	for target_label in range(rows*cols): # PER CLASS
		ax = axs[int(target_label/cols),target_label%cols]
		if target_label<len(class_names):
			target_name = class_names[target_label]
			labels = np.argmax(y_test[:len(full_s),0,:],axis=-1)
			indexs = np.where(labels==target_label)[0]
			target_class = class_names[target_label]
			class_s = full_s[indexs]
			class_s = class_s[:100]
			lens = full_output_len[indexs]
			lens = lens[:100]
			#print(class_s.shape)
			#print('class_s',class_s.shape)

			if mode=='all':
				all_heads = normalize_heads_per_batch(class_s, head_size, crop_time)
				#all_heads = class_s

			if mode=='one':
				class_s = class_s[:,target_head,:][:,np.newaxis,:]
				all_heads = normalize_heads_per_batch(class_s, 1, crop_time)

			if mode=='mean':
				class_s = np.mean(class_s,axis=1)[:,np.newaxis,:]
				all_heads = normalize_heads_per_batch(class_s, 1, crop_time)

			#print('all_heads:',all_heads.shape)
			all_heads = gray_to_viridis(all_heads, lens)
			all_heads = np.reshape(all_heads, (all_heads.shape[0], all_heads.shape[1]*all_heads.shape[2],-1))

			#ax.imshow(all_heads, extent=[0,100,0,200])
			ax.imshow(all_heads, extent=[0,max_length,0,max_length*2])
			ax.set_xlabel('sequence length: {}'.format(3+step_index))
			ax.set_title('class: {} ({})'.format(target_label, target_class, len(indexs)))
			if target_label%cols==0:
				ax.set_ylabel('class batch sample')
		else:
			ax.set_axis_off()
		plt.suptitle('{}\nlayer: {}'.format(model_name,attention_layer), fontsize=20)
		fig.tight_layout()

	if save_image:
		#save_folder
		#save_folder = 'attention_maps/{}/layer{}_{}'.format(myModel.model_name, attention_layer, mode)
		create_folders(save_folder)
		#plt.imsave('{}/att_time{}.jpg'.format(save_folder,step_index),all_heads)
		filename = '{}/attn_time{:03d}.jpg'.format(save_folder,step_index)
		print(filename)
		plt.savefig(filename)
		plt.close()
	plt.show()