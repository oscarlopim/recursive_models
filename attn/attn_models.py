from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F
from attn_layers import AttentionOverPast
import multihead_attn
from rnn_utils import FinalLinear
from tinyFlame.models import add_model_info

def count_parameters(model):
	return sum(p.numel() for p in model.parameters() if p.requires_grad)

def get_encoding(max_len, frec_n):
	waves = np.empty((1,max_len,frec_n*2),dtype=np.float32)
	for i in range(0,frec_n):
		#i=4
		radian = np.arange(0,max_len).astype(np.float32)*(2*np.pi*((i+1)/max_len))
		sin = np.sin(radian)
		cos = np.cos(radian)
		#sin = (sin>=0).astype(np.float32);cos = (cos>=0).astype(np.float32)
		waves[0,:,i*2] = sin
		waves[0,:,i*2+1] = cos
	return waves

frec_n = 8; encoding_dim = 4
#frec_n = 8; encoding_dim = 6
#encoding_dim = frec_n*2



class CNN1dAttnOP(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, max_time_steps, kernel_size=5, output_features=None, dropout=0, use_cat=False):
		super(CNN1dAttnOP, self).__init__()
		self.input_features = input_features
		#dim_per_head = 64
		self.output_features = dim_per_head

		#kernel_size = 7 # 3 5 7
		print('input_features',input_features,'kernel_size',kernel_size)
		self.cnn1d = nn.ModuleList()
		self.cnn1d.append(nn.Conv1d(in_channels=input_features, out_channels=dim_per_head-encoding_dim, kernel_size=1));print('cnn1d',count_parameters(self.cnn1d[0]))
		self.cnn1d.append(nn.Conv1d(in_channels=dim_per_head-encoding_dim, out_channels=dim_per_head-encoding_dim, kernel_size=kernel_size));print('cnn1d',count_parameters(self.cnn1d[1]))
		self.cnn1d.append(nn.Conv1d(in_channels=dim_per_head-encoding_dim, out_channels=dim_per_head-encoding_dim, kernel_size=kernel_size));print('cnn1d',count_parameters(self.cnn1d[2]))
		
		self.pad = nn.ConstantPad1d([kernel_size-1,0], 0)

		self.pad_pool = nn.ConstantPad1d([1,0], -1e21)
		self.pool = nn.MaxPool1d(kernel_size=2, stride=1)

		use_residual = 1
		self.attOP = AttentionOverPast(self.output_features, head_size, dim_per_head, max_time_steps,output_features=self.output_features, dropout=dropout, use_cat=use_cat, use_residual=use_residual)
		self.output_features = self.attOP.get_output_dims()

		self.dropout = nn.Dropout(dropout);self.fc1 = nn.Linear(self.output_features,self.output_features);self.fc2 = nn.Linear(self.output_features,self.output_features)

		self.linear = PLinear(frec_n*2, encoding_dim, dropout)

	def get_output_dims(self):
		return self.output_features

	def last_fc(self, input): # al parecer importa...
		x = self.fc1(self.dropout(input))
		x = F.relu(x)
		x = self.fc2(self.dropout(x))
		x = x+input
		x = F.relu(x)
		return x

	def forward(self, x, x_lengths, encoding):
		#print('x',x.shape)
		x = x.transpose(-2,-1)
		#print(x.shape)
		x = self.cnn1d[0](x)
		x = F.relu(x)
		#print(x.shape)

		xx = x
		for k in range(1,len(self.cnn1d)):
			xx = self.cnn1d[k](self.pad(xx));xx = F.relu(xx)
		#x = self.pool(self.pad_pool(x))
		#print(x.shape)
		#print(xx.shape)
		x = xx+x

		x = x.transpose(-2,-1)
		

		''' FUNCA PERO SACAR DE ACA
		one_hot = torch.tensor([0,0,1,1,0,1,0,1,0,1])
		indexs = torch.nonzero(one_hot).squeeze()
		#print(indexs)

		#print('x',x[0,:,0])
		indexs = torch.tensor([0,5,7,9,10,23])
		zz = torch.zeros_like(x)
		xx = x[:,indexs,:]
		#print('xx',xx[0,:,0])
		#indexs = torch.tensor([0,5,7,9,10,23])
		
		zz[:,indexs,:] = xx

		#print('zz',zz[0,:,0])

		x = zz
		'''

		encoding = self.linear(encoding)
		x = torch.cat([x, encoding],dim=-1)
		#x = torch.cat([x,x],dim=-1)
		#print(x.shape)
		
		x_att, last_x_att, s, last_s = self.attOP(x, x_lengths)
		#x_att, last_x_att, s, last_s = x, x[:,-1,:], x, x[:,-1,:]
		
		x_att = self.last_fc(x_att);last_x_att = self.last_fc(last_x_att)
		
		x = x_att
		x_last = last_x_att
		return x, x_last, s, last_s

#################################################################################################3
#################################################################################################3

class PLinear(nn.Module):
	def __init__(self, input_features, attention_features, dropout):
		super(PLinear, self).__init__()
		self.dropout = nn.Dropout(dropout)
		self.fc1 = nn.Linear(input_features, input_features)
		self.fc2 = nn.Linear(input_features, attention_features)

	def forward(self, x):
		return self.forward_relu(x)
		#return self.forward_tanh(x)

	def forward_relu(self, x):
		x = F.relu(self.fc1(self.dropout(x)))
		x = self.fc2(self.dropout(x))
		return x

	def forward_tanh(self, x):
		x = torch.tanh(self.fc1(self.dropout(x)))
		x = torch.tanh(self.fc2(self.dropout(x)))
		return x

class FullAttentionModel(nn.Module):
	def __init__(self, model_parameters):
		super(FullAttentionModel, self).__init__()

		self.model_parameters = model_parameters
		input_dim = model_parameters['input_dim']-1 # last is length
		latent_dim = model_parameters['latent_dim']
		output_dim = model_parameters['n_classes']
		rnn_layers = model_parameters['latent_layers']
		dropout = model_parameters['dropout']
		rnn_cell= model_parameters['rnn_class']
		max_time_steps = model_parameters['max_time_steps']
		attn_heads = model_parameters['attn_heads']
		use_cat = model_parameters['attn_use_cat']

		self.mode = 'FAttnOP'
		print('attn mode:',self.mode)

		self.attention = nn.ModuleList()

		heads = 4 # 3
		#units = np.ones(3,dtype=int)*10
		units = np.power(2,np.arange(0,heads))*20
		#units = np.ones(heads,dtype=int)*12 # 10, 
		#units = attn_heads*2

		#kernel_sizes = [9,7,5]
		kernel_sizes = [7,7,5]

		attention_dim = input_dim
		for k,head_size in enumerate(attn_heads):
			print('-'*60)
			(head_size, dim_per_head) = (head_size, units[k]) # (4, units) # 6, 15
			attention = CNN1dAttnOP(attention_dim, head_size, dim_per_head, max_time_steps, kernel_sizes[k], dropout=dropout, output_features=dim_per_head, use_cat=use_cat)
			attention_dim = attention.get_output_dims()
			self.attention.append(attention)
			#print('attention_dim:',attention_dim)
		
		self.last_fc = FinalLinear(attention_dim, output_dim, dropout)
		self.get_name()

		#self.encoding = torch.as_tensor(get_encoding(max_time_steps, frec_n))
		self.encoding = nn.Parameter(torch.as_tensor(get_encoding(max_time_steps, frec_n)), requires_grad=False)
		#self.x_positional_encoding = self.encoding.expand(256,-1,-1) # repeat copies, expand dont
		#self.x_positional_encoding = nn.Parameter(self.x_positional_encoding,requires_grad=False)
		#print('self.encoding',self.encoding.shape)
		#print(self.encoding.transpose(-2,-1))

	def print(self):
		print('model_parameters:',self.model_parameters)

	def get_name(self):
		mp = self.model_parameters

		name = ''
		name += add_model_info('mdl',self.mode)
		name += add_model_info('ch',''.join(mp['channel_names']))
		#name += add_model_info('dout',mp['dropout'])
		name += add_model_info('L',mp['latent_layers'])
		name += add_model_info('U',mp['latent_dim'])
		name += add_model_info('AttnU',mp['attn_dim'])
		name += add_model_info('AttnH',mp['attn_heads'])
		name += add_model_info('AttnFun',multihead_attn.ATTN_TYPE)
		name += add_model_info('AttnTranf',multihead_attn.ATTN_LINEAR)

		name = name[1:] # delete first atribute!! -> -
		self.name = name

	def forward(self, x_and_lengths):
		x_and_lengths = x_and_lengths.float()
		x = x_and_lengths[:,:,:-1]
		#x_lengths = x_and_lengths[:,0,-1].cpu().numpy() # NUMPY
		x_lengths = x_and_lengths[:,0,-1].long() # TENSOR
		#x_lengths = x_lengths.unsqueeze(1)
		#print("x",x.shape)
		#print("x_lengths",x_lengths)
		#print("x_lengths",x_lengths.shape)

		#self.packing = True
		self.packing = x_lengths[0]>0 # vienen secuencias de largo 0 en build
		
		batch_size = x.shape[0]
		#self.x_positional_encoding = self.encoding
		self.x_positional_encoding = self.encoding.expand(batch_size, -1, -1).requires_grad_(False) # expand dont copies
		#self.x_positional_encoding = self.encoding.repeat(batch_size, 1, 1).requires_grad_(False) # repeat copies

		last_s = [] # podria tener problemas con la memoria en train
		for i in range(0,len(self.attention)):
			#print('in',i,x.shape)
			x, last_x_att, s, last_s_ = self.attention[i](x, x_lengths, self.x_positional_encoding)
			last_s.append(last_s_)
			#x = self.dropout(x)
			#last_x_att = self.dropout(last_x_att) # use this?
			
			#print('out',i,x.shape)

		#print('x',x.shape);print('x',x[0,:,0])
		#print('last_x_att',last_x_att.shape);print('last_x_att',last_x_att[0,0])
		#print('last_s',last_s[0,0,:]) # head: 0
		
		#x, last_ht = self.attention2(x)

		y_pred = self.last_fc(x)
		#x_lengths = x_lengths.to('cpu')
		#print(y_pred.shape)
		
		if self.packing:
			if not self.training: # in val mode
				last_y_pred = self.last_fc(last_x_att)
				#print("y_pred",y_pred.shape)
				#print("last_y_pred",last_y_pred.shape)
				#print("y_pred",y_pred[0,:,0])
				#print("last_y_pred",last_y_pred[0,0])
				return y_pred, x_lengths, last_y_pred, last_s, s
			
			return y_pred, x_lengths, s
		return y_pred # if not packing