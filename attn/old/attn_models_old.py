from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn

class AttentionModel(nn.Module):
	def __init__(self, model_parameters):
		super(AttentionModel, self).__init__()

		self.model_parameters = model_parameters
		input_dim = model_parameters['input_dim']-1 # last is length
		latent_dim = model_parameters['latent_dim']
		output_dim = model_parameters['n_classes']
		rnn_layers = model_parameters['latent_layers']
		dropout = model_parameters['dropout']
		rnn_cell= model_parameters['rnn_class']

		#self.mode = 'AttOP' # 'AttentionOverPast'
		self.mode = 'RAttnOP' # 'RecursiveFeatures_AttentionOverPast'
		print('attn mode:',self.mode)

		self.attention = nn.ModuleList()
		'''
		if self.mode == 'AttentionOverPast':
			head_size = 1
			units = 100

			(head_size, dim_per_head) = (head_size, units)
			attention = RecursiveFeatures_AttentionOverPast(input_dim, head_size, dim_per_head, dropout=dropout)
			#attention = RecursiveFeatures_AttentionOverPast(input_dim, head_size, dim_per_head, dropout=dropout, use_lstm=False)
			attention_dim = attention.get_output_dims()
			self.attention.append(attention)
			print(attention_dim)

			units = 50
			(head_size, dim_per_head) = (head_size, units)
			attention = AttentionOverPast(attention_dim, head_size, dim_per_head, dropout=dropout)
			attention_dim = attention.get_output_dims()
			self.attention.append(attention)
			print(attention_dim)

			(head_size, dim_per_head) = (head_size, units)
			attention = AttentionOverPast(attention_dim, head_size, dim_per_head, dropout=dropout)
			attention_dim = attention.get_output_dims()
			self.attention.append(attention)
			print(attention_dim)
		'''
		if self.mode == 'RAttnOP':
			attn_heads = model_parameters['attn_heads']
			units = model_parameters['attn_dim']

			head_size = attn_heads[0]
			(head_size, dim_per_head) = (head_size, units) # (4, units) # 6, 15
			attention = RecursiveFeatures_AttentionOverPast(input_dim, head_size, dim_per_head, rnn_cell, dropout=dropout, layers=rnn_layers)
			attention_dim = attention.get_output_dims()
			self.attention.append(attention)
			print('attention_dim:',attention_dim)

			for head_size in attn_heads[1:]:
				print('-'*60)
				(head_size, dim_per_head) = (head_size, units) # (4, units) # 6, 15
				attention = AttentionOverPast(attention_dim, head_size, dim_per_head, dropout=dropout, output_features=dim_per_head)
				attention_dim = attention.get_output_dims()
				self.attention.append(attention)
				print('attention_dim:',attention_dim)
		
		self.dropout = nn.Dropout(dropout)
		#self.fc1 = nn.Linear(attention_dim, attention_dim)
		self.fc1 = nn.Linear(attention_dim, output_dim)
		self.relu = nn.ReLU()
		self.softmax = nn.Softmax(dim=-1) # usar log?
		self.get_name()

	def print(self):
		print('model_parameters:',self.model_parameters)

	def get_name(self):
		mp = self.model_parameters
		name = self.mode+'_'
		name += "".join(mp['channel_names'])
		#name += '_norm-{}'.format(mp['normalization'])
		name += '_dout{}'.format(int(mp['dropout']*100))
		name += '_L{}'.format(mp['latent_layers'])
		name +='_U{}'.format(mp['latent_dim'])
		name += '_AttnU{}'.format(mp['attn_dim'])
		name += '_AttnH{}'.format(mp['attn_heads'])
		name +='_{}'.format(ATTN_TYPE)
		name +='_{}'.format(ATTN_FUN)
		#print('name',name)
		self.name = name

	def last_fc(self, x):
		#x = self.relu(self.fc1(x))
		x = self.fc1(x)
		#print('fc1',self.fc1.weight.max().data.item()) # no veo nada raro
		x = self.softmax(x)
		return x

	def forward(self, x_and_lengths):
		x_and_lengths = x_and_lengths.float()
		x = x_and_lengths[:,:,:-1]
		#x_lengths = x_and_lengths[:,0,-1].cpu().numpy() # NUMPY
		x_lengths = x_and_lengths[:,0,-1].long() # TENSOR
		#x_lengths = x_lengths.unsqueeze(1)
		#print("x",x.shape)
		#print("x_lengths",x_lengths)
		#print("x_lengths",x_lengths.shape)
		self.packing = x_lengths[0]>0 # vienen secuencias de largo 0 en build
		
		last_s = [] # podria tener problemas con la memoria en train
		for i in range(0,len(self.attention)):
			#print('in',i,x.shape)
			x, last_x_att, s, last_s_ = self.attention[i](x, x_lengths)
			last_s.append(last_s_)
			x = self.dropout(x)
			last_x_att = self.dropout(last_x_att) # use this?
			#print('out',i,x.shape)

		#print('x',x.shape);print('x',x[0,:,0])
		#print('last_x_att',last_x_att.shape);print('last_x_att',last_x_att[0,0])
		#print('last_s',last_s[0,0,:]) # head: 0
		
		#x, last_ht = self.attention2(x)

		y_pred = self.last_fc(x)
		x_lengths = x_lengths.to('cpu')
		#print(y_pred.shape)
		
		if self.packing:
			if not self.training: # in val mode
				last_y_pred = self.last_fc(last_x_att)
				#print("y_pred",y_pred.shape)
				#print("last_y_pred",last_y_pred.shape)
				#print("y_pred",y_pred[0,:,0])
				#print("last_y_pred",last_y_pred[0,0])
				return y_pred, x_lengths, last_y_pred, last_s, s
			
			return y_pred, x_lengths, s
		return y_pred # if not packing

#################################################################################################3
#################################################################################################3
