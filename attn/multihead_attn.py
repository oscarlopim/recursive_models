from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np

#ATTN_TYPE = 'cos' # default is cosine - more stable??
ATTN_TYPE = 'sdot'

#ATTN_FUN = 'sigmoid' # SIGMOID # da pesimo
ATTN_FUN = 'softmax' # SOFTMAX

ATTN_LINEAR = 'PLinearSimple'
#ATTN_LINEAR = 'PLinear'

def subsequent_mask(size):
	"Mask out subsequent positions."
	attn_shape = (1, size, size)
	subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
	return torch.from_numpy(subsequent_mask) == 0

#######################################################################################33

class PLinearSimple(nn.Module):
	def __init__(self, input_features, attention_features, dropout):
		super(PLinearSimple, self).__init__()
		self.dropout = nn.Dropout(dropout)
		self.fc1 = nn.Linear(input_features, attention_features)

	def forward(self, x):
		x = self.fc1(self.dropout(x))
		return x

class PLinear(nn.Module):
	def __init__(self, input_features, attention_features, dropout):
		super(PLinear, self).__init__()
		self.dropout = nn.Dropout(dropout)
		self.fc1 = nn.Linear(input_features, input_features)
		self.fc2 = nn.Linear(input_features, attention_features)

	def forward(self, x):
		#return self.forward_relu(x)
		return self.forward_tanh(x)

	def forward_relu(self, x):
		x = F.relu(self.fc1(self.dropout(x)))
		x = self.fc2(self.dropout(x))
		return x

	def forward_tanh(self, x):
		x = torch.tanh(self.fc1(self.dropout(x)))
		x = torch.tanh(self.fc2(self.dropout(x)))
		return x

def get_PLinear(class_name):
	if class_name=='PLinear':
		return PLinear
	if class_name=='PLinearSimple':
		return PLinearSimple
	raise Exception('No class with this name: '+class_name)

#######################################################################################33

class MultiHeadAttentionContext(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, max_length_time, scores_dropout=0.0, ignored_steps_indices=None):
		super(MultiHeadAttentionContext, self).__init__()
		self.input_features = input_features
		self.attention_features = head_size*dim_per_head
		print('attention_features:{} head_size:{} dim_per_head:{}'.format(self.attention_features,head_size,dim_per_head))
		self.head_size = head_size
		self.output_features = self.attention_features
		
		dropout = 0.2 # 0.2 es muy alto?

		#self.v_linear = PreLinear_basic(self.input_features, self.attention_features, dropout)
		PreLinear_class = get_PLinear(ATTN_LINEAR)
		self.v_linear = PreLinear_class(self.input_features, self.attention_features, dropout)
		self.k_linear = PreLinear_class(self.input_features, self.attention_features, dropout)
		self.q_linear = PreLinear_class(self.input_features, self.attention_features, dropout)

		self.scores_dropout = nn.Dropout(scores_dropout) # WHAT
		self.fast_mode = True # OJO con esto
		self.attn_mode = ATTN_TYPE
		print('attn_mode:',self.attn_mode)
		#self.mask = subsequent_mask(max_length_time)
		mask = subsequent_mask(max_length_time)
		self.mask = nn.Parameter(mask,requires_grad=False)
		#self.mask = torch.as_tensor(subsequent_mask(max_length_time)).to(self.v_linear.device)

	def get_output_dims(self):
		return self.output_features

	def forward(self, input):
		return self.forward_mh(input, input, input)

	def forward_mh(self, queries, keys, values):
		b = values.shape[0]
		t = values.shape[1]
		q_size = queries.shape[1]
		h = self.head_size
		#print('queries',queries.shape);print('keys',keys.shape);print('values',values.shape)
		debug = 0

		if not debug:
			queries = self.q_linear(queries) # queries = bxqxn
			keys = self.k_linear(keys) # keys = bxtxn
			values = self.v_linear(values) # values = bxtxn
			pass
		#print('queries',queries.shape);print('keys',keys.shape);print('values',values.shape)

		#queries = self.tanh(queries);keys = self.tanh(keys);values = self.tanh(values) # TANH - no es tan util en realidad... mp sirve para mayor dim, ni para multiHead

		#.view(b,h,q_size,-1) # bxhxqxn
		#ql = queries.view(b,h,-1,q_size) # bxhxqxn
		#print('ql',ql.shape)

		#kl = self.k_linear(keys).view(b,h,-1,t) # bxhxnxt
		#kl = keys.view(b,h,-1,t) # bxhxtxn
		vl = values.view(b,h,t,-1) # bxhxtxn

		#print('ql',ql[0,0,:,:])
		#print('kl',kl[0,0,:,:])

		if (not self.fast_mode): # SLOW mode # deberia hacer algo???
			ql = queries.view(b,h,-1,q_size) # bxhxnxq
			kl = keys.view(b,h,t,-1) # bxhxtxn
			
			#scores = torch.matmul(ql, kl) # bxhxqx1xn * bxhxqxnxt = bxhxqxt
			scores = torch.matmul(kl,ql) # bxhxtxn * bxhxnxq = bxhxtxq
			#print(scores.max(),scores.min())
			#print(scores.min())

		else: # FAST mode
			ql = queries.view(b,h,q_size,-1) # bxhxqxn
			kl = keys.view(b,h,t,-1) # bxhxtxn
			#print('kl',kl[0,0,:,:])
			#print('ql',ql[0,0,:,:])

			
			#print(ql.min(),ql.max())

			inf_value = 1e21

			if self.attn_mode=='cos':
				ql = torch.transpose(ql,-1,-2) # bxhxqxn -> bxhxnxq
				norm_kl = F.normalize(kl, p=2, dim=-1)
				norm_ql = F.normalize(ql, p=2, dim=-2)
				#print('norm_kl',torch.norm(norm_kl,p=2, dim=-1))
				#print('norm_ql',torch.norm(norm_ql,p=2, dim=-2))
				scores = torch.matmul(norm_kl,norm_ql) # bxhxtxn * bxhxnxq = bxhxtxq
				#inf_value = 1e9

			if self.attn_mode=='dot':
				ql = torch.transpose(ql,-1,-2) # bxhxqxn -> bxhxnxq
				scores = torch.matmul(kl,ql) # bxhxtxn * bxhxnxq = bxhxtxq 

			if self.attn_mode=='sdot':
				ql = torch.transpose(ql,-1,-2) # bxhxqxn -> bxhxnxq
				scores = torch.matmul(kl,ql)/math.sqrt(kl.shape[-1]) # bxhxtxn * bxhxnxq = bxhxtxq 
				#scores = torch.clamp(scores, min=-10, max=10)
			
			#inf_value = float('-inf')
			#print('inf_value',inf_value)

			#ind1, ind2 = torch.triu_indices(t,t,offset=0) # kill self attention # se rompe todo :(, no se por que, da como 94% altiro en mean... ya se por que pasa
			#ind1, ind2 = torch.triu_indices(t,t,offset=1)
			
			#print('self.mask',self.mask.shape,self.mask)
			
			scores = scores.masked_fill(self.mask==0, -inf_value)

			'''
			ignored_steps_indices = [0,1,2,5,6]
			if not ignored_steps_indices is None:
				scores[:,:,:,ignored_steps_indices] = -inf_value

			first_consecutive_ignored = []
			last_i = -1
			for i in ignored_steps_indices: # ignored_steps_indices MUST BE SORTED
				if not i-last_i==1:
					break
				first_consecutive_ignored.append(i)
				last_i = i
			'''

		#print('q_linear',self.q_linear.weight.max().data.item(),'k_linear',self.k_linear.weight.max().data.item(),'v_linear',self.v_linear.weight.max().data.item()) # no veo problemas
		#print('q_linear',self.q_linear.weight.min().data.item(),'k_linear',self.k_linear.weight.min().data.item(),'v_linear',self.v_linear.weight.min().data.item()) # no veo problemas

		#print('scores',scores.shape,scores[0,0,:10,:10])
		scores = scores.view(b,h,-1,t) # bxhxtxq -> bxhxqxt
		#scores = torch.transpose(scores,-1,-2) # bxhxtxq -> bxhxqxt

		#soft = scores

		if ATTN_FUN=='sigmoid':
			aligments = F.sigmoid(scores) # SIGMOID bxhxqxt
			pass
		elif ATTN_FUN=='softmax':
			aligments = F.softmax(scores, dim=-1) # SOFTMAX bxhxqxt
		else:
			raise Exception('no attn fun')
		
		aligments_drop = aligments
		#aligments_drop = self.scores_dropout(aligments) # SCORES DROPOUT - WHAT?
		#print('soft',soft.shape,soft[0,0,:10,:10])

		output = torch.matmul(aligments_drop, vl) # bxhxqxt * bxhxtxn = bxhxqxn
		output = output.contiguous().view(b,q_size,-1) # bxhxqxn -> bxqx(hxn) # <- CAT!!
		#print('output',output.shape,output.max().data.item(),output.min().data.item()) # CRECE!!! SUPER UTIL
		return output, aligments # bxqx(hxn) , bxhxqxt

#################################################################################################3
#################################################################################################3

class LayerNorm(nn.Module):
	"Construct a layernorm module (See citation for details)."
	def __init__(self, features, eps=1e-4):
		super(LayerNorm, self).__init__()
		self.a_2 = nn.Parameter(torch.ones(features))
		self.b_2 = nn.Parameter(torch.zeros(features))
		self.eps = eps

	def forward(self, x, lens):
		# METER PARCHE DE IGNORAR SI SON TODOS 0?
		#print('x',x.shape)
		#print('x',x[:3,-20:,0])
		#print('lens',lens[:3])
		mean = x.mean(-1, keepdim=True)
		std = x.std(-1, keepdim=True)
		norm = self.a_2 * (x - mean) / (std + self.eps) + self.b_2
		return norm

	def forward___(self, x, lens):
		# METER PARCHE DE IGNORAR SI SON TODOS 0?
		print('x',x.shape)
		print('x',x[:3,-20:,0])
		print('lens',lens[:3])
		mean = x.mean(-1, keepdim=True)
		std = x.std(-1, keepdim=True)
		norm = self.a_2 * (x - mean) / (std + self.eps) + self.b_2
		return norm

class MultiHeadAttnCell(nn.Module):
	def __init__(self, input_features, head_size, dim_per_head, max_length_time, output_features=None, dropout=0, use_cat=False, use_residual=False):
		super(MultiHeadAttnCell, self).__init__()

		self.input_features = input_features
		self.attention_features = head_size*dim_per_head
		self.head_size = head_size
		self.attn = MultiHeadAttentionContext(input_features, head_size, dim_per_head, max_length_time)
		
		#self.add_final_fc = True
		self.add_final_fc = False
		print('add_final_fc:',self.add_final_fc)

		self.output_features = output_features
		if self.output_features is None: # output_dim is NOT defined
			print('-> self.output_features is None!!')
			self.output_features = self.attention_features
		print('output_features:',output_features)


		self.use_cat = use_cat # cat input with output from attn
		print('use_cat:',self.use_cat)
		if self.use_cat:
			self.output_features = self.attention_features+self.input_features

		if self.add_final_fc: # arreglar??
			self.dropout = nn.Dropout(dropout)
			if self.use_cat:
				fc1_in = self.attention_features+self.input_features
			else:
				fc1_in = self.attention_features
			#self.fc1 = nn.Linear(fc1_in, fc1_in) # BIG
			self.fc1 = nn.Linear(fc1_in, self.output_features) # SMALL
			self.fc2 = nn.Linear(self.output_features, self.output_features)
			self.cat_norm = LayerNorm(fc1_in)

		self.input_norm = LayerNorm(self.input_features)
		self.use_batchnorm = False # batchnorm in input and pre-cat to finalFC
		self.use_residual = use_residual
		print('use_batchnorm:',self.use_batchnorm)
		print('use_residual:',self.use_residual)
		
	def get_output_dims(self):
		return self.output_features

	def fully_connected_forward(self, input):
		input = F.tanh(self.fc1(self.dropout(input))) # just because last one was a tanh
		input = self.fc2(self.dropout(input))
		return input

	def forward(self, input, lens):
		if self.use_batchnorm:
			input = self.input_norm(input, lens)
		output_att, scores = self.attn(input)
		
		if self.use_residual:
			#print('input',input.shape,'output_att',output_att.shape)
			output_att = input + output_att # RESIDUAL

		if self.use_cat:
			output_att = torch.cat([output_att,input],dim=-1)

		if self.add_final_fc:
			if self.use_batchnorm:
				#output_att = self.cat_norm(output_att, lens)
				pass

			output = self.fully_connected_forward(output_att) # FC

		#if self.use_residual:
			#output2 = output1 + output2 # RESIDUAL
		#	pass
		return output_att, scores



'''
if self.attn_mode=='correntropy':
# ql = bxhxqxn
# kl = bxhxtxn
ql = ql.unsqueeze(3)#.repeat(1,1,1,3,1) # bxhxqxn -> # bxhxqxtxn
kl = kl.unsqueeze(2)#.repeat(1,1,3,1,1) # bxhxtxn -> # bxhxqxtxn
#print('ql',ql.shape,ql[0,0,:,0])
#print('kl',kl.shape,kl[0,0,:,0])
#std = 10
x = kl-ql

dim = -1
d = kl.shape[-1]
d = 1
N = ql.shape[dim]
eps=1e-4
std_x = x.std(dim=dim)
#print('std_x',std_x.shape)
#print('std_x',std_x[0,:,:])

#fac = (4/N/(2*d+1))**(1/d+4) # very small
#fac = 1
fac = 1.0592*N**(-1/5)

std = std_x*fac+eps
std = std.unsqueeze(-1)
#print('std',std.shape,std[0,:,:])
#print('x',x.shape,x[0,0,:,:])
scores = torch.mean(torch.exp(-x.pow(2)/(2*std.pow(2))),dim=-1) #  = bxhxtxq
#scores = scores/ql.shape[-1]
#print('scores',scores.shape)

'''