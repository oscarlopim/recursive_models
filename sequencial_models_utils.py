from __future__ import print_function
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F 
import math
import numpy as np

class SerialParallelCurveHandler(nn.Module):
	def __init__(self, bands_count, alive_p=0.05):
		super().__init__()
		self.bands = bands_count
		
		# DROP DROPOUT IN SERIAL
		self.alive_p = alive_p
		self.bernoulli = torch.distributions.Bernoulli(torch.tensor([1-self.alive_p]))

	def randomCurveAgumentation(self, x_onehots):
		if self.training and self.alive_p>0:
			device = x_onehots.device
			sample_shape = (x_onehots.shape[1],)
			p_indexs = self.bernoulli.sample(sample_shape=sample_shape).to(device).byte()[:,0]
			#print('p_indexs',p_indexs.shape,p_indexs)
			idx = torch.nonzero(p_indexs)
			new_x = torch.zeros_like(x_onehots, device=device)
			new_x[:,:len(idx),:] = x_onehots[:,idx[:,0],:] # SLOW?
			return new_x
		
		return x_onehots
		
	def mapping(self, source, indexs, output, dim=1):
		fixed_indexs = indexs.clone()
		IMD = source.shape[1]
		fixed_indexs[fixed_indexs==IMD] = output.shape[dim]-1
		fixed_indexs = fixed_indexs.unsqueeze(-1).expand(-1,-1,source.shape[-1])
		#print(output.device, fixed_indexs.device, source.device)
		output.scatter_(dim, fixed_indexs, source)

	def serial_to_parallel(self, x_onehots):
		device = x_onehots.device
		band_to_print = 0
		x_serial = x_onehots[:,:,:-self.bands]
		onehot_serial = x_onehots[:,:,-self.bands:].byte()
		#print('x_serial', x_serial.shape)
		#print('onehot_serial', onehot_serial.shape, torch.transpose(onehot_serial,-2,-1))
		EXTRA = 1
		parallel_holder_list = []
		parallel_lengths_list = []
		parallel_onehot_list = []
		p2s_mapping_indexs_list = []
		for kb in range(self.bands):
			#print(str(kb)+'='*80)
			onehot_band = onehot_serial[:,:,kb]
			parallel_onehot_list.append(onehot_band)
			#print(onehot_band[0])
			lengths = onehot_band.sum(dim=-1)
			#print(lengths.shape, lengths)
			parallel_lengths_list.append(lengths)

			parallel_holder = torch.zeros((x_serial.shape[0], x_serial.shape[1]+EXTRA, x_serial.shape[2]), device=device)
			IMD = x_onehots.shape[1]
			p2s_mapping_indexs = torch.full((x_serial.shape[0], x_serial.shape[1]+EXTRA,1),IMD, device=device)
			#print('onehot_band',onehot_band.shape,onehot_band)
			
			s2p_mapping_indexs = (torch.cumsum(onehot_band, 1)-1).masked_fill(~onehot_band, IMD)
			#print('s2p_mapping_indexs', s2p_mapping_indexs.shape, s2p_mapping_indexs)
			
			self.mapping(x_serial, s2p_mapping_indexs, parallel_holder)
			parallel_holder = parallel_holder[:,:-1,:]
			#print('parallel_holder', parallel_holder.shape, parallel_holder[:,:,1])
			parallel_holder_list.append(parallel_holder)

			arange = torch.arange(0,x_serial.shape[1], device=device).unsqueeze(0).expand(x_serial.shape[0],-1)
			arange = arange.unsqueeze(-1).float()
			self.mapping(arange, s2p_mapping_indexs, p2s_mapping_indexs)
			p2s_mapping_indexs = p2s_mapping_indexs[:,:-1,0].long()
			#print('p2s_mapping_indexs', p2s_mapping_indexs.shape, p2s_mapping_indexs)
			p2s_mapping_indexs_list.append(p2s_mapping_indexs)
			#print('-'*80)
		
		return parallel_holder_list, parallel_lengths_list, parallel_onehot_list, p2s_mapping_indexs_list, onehot_serial
		
	def parallel_to_serial(self, parallel_holder_list, parallel_lengths_list, p2s_mapping_indexs_list):
		x_serial = None
		for kb in range(self.bands):
			parallel_holder = parallel_holder_list[kb]
			p2s_mapping_indexs = p2s_mapping_indexs_list[kb]
			device = parallel_holder.device
			if x_serial is None:
				x_serial = torch.zeros_like(parallel_holder, device=device)
			
			self.mapping(parallel_holder, p2s_mapping_indexs, x_serial)

		lengths = torch.cat([p.unsqueeze(-1) for p in parallel_lengths_list], dim=-1).sum(1)
		return x_serial, lengths

'''
def f(x):
	return 2*x
	
bands_count = 2
alive_p = 5e-2
spCH = SerialParallelCurveHandler(bands_count, alive_p)

ind = 42
x_onehots = torch.tensor(x[ind:ind+2])
#print('x_onehots', x_onehots.shape, x_onehots[:,:,1])
x_onehots = spCH.randomCurveAgumentation(x_onehots)
print('x_onehots', x_onehots.shape, x_onehots[:,:,1])
print('#'*60)
parallel_holder_list, p2s_mapping_indexs_list = spCH.serial_to_parallel(x_onehots)
parallel_results = []
for pp in parallel_holder_list:
	res = f(pp)
	parallel_results.append(res)
	
print('#'*60)
x_serial = spCH.parallel_to_serial(parallel_results, p2s_mapping_indexs_list)
print('x_serial', x_serial.shape, x_serial[:,:,1])
'''


class PositionalEncoding(nn.Module):
	def __init__(self, frequencies, max_period, max_curve_length, dropout=0.0):
		super().__init__()
		self.frequencies = frequencies
		self.features = frequencies*2
		self.max_period = max_period
		self.max_curve_length = max_curve_length
		self.dropout = dropout
		self.periods = []
		#increase_factor = 1
		for k_ in range(0,self.frequencies):
			k = 2*k_
			p = self.max_period/(k+1)
			#p = self.max_period**(2*(k+1)/self.features)
			self.periods.append(p)

		#self.periods = np.

		self.dropout_f = nn.Dropout(self.dropout)

		self.delete_time_offset = True
		self.save_train_time_window = 0
		self.saved_max_time_window = -float('inf')
		self.saved_min_time_window = float('inf')

	def extra_repr(self):
			return 'features={}, frequencies={}, max_period={}, dropout={}'.format(
				self.features, self.frequencies, self.max_period, self.dropout
			)

	def __repr__(self):
		txt = 'PositionalEncoding({})'.format(self.extra_repr())
		txt += '\n\t -Periods:{}'.format([round(p,4) for p in self.periods])
		return txt

	def forward(self, x_time, lengths):
		'''
		time bxtx1
		'''
		#x_time = torch.linspace(0,self.max_period,x_time.shape[1]).unsqueeze(0).expand(x_time.shape[0],-1) # test
		#print('lengths',lengths.shape,lengths[0])
		#print('x_time',x_time.shape,x_time[0])
		if self.delete_time_offset:
			x_time = x_time - x_time[:,0].unsqueeze(-1)
		#print('x_time',x_time.shape,x_time[0])
		
		if self.save_train_time_window and self.training:
			max_value = torch.max(x_time)
			min_value = torch.min(x_time[:,1])
			if max_value>self.saved_max_time_window:
				self.saved_max_time_window = max_value

			if min_value<self.saved_min_time_window:
				self.saved_min_time_window = min_value

			print('saved_min_time_window:{} - saved_max_time_window:{}'.format(self.saved_min_time_window, self.saved_max_time_window))

		encoding = x_time.unsqueeze(-1).repeat(1,1,self.features)
		#print('encoding',encoding.shape, lengths.shape)
		for k,p in enumerate(self.periods):
			w = 2*math.pi/p
			sin = torch.sin(x_time*w)
			cos = torch.cos(x_time*w)
			#print('w',w.shape,w[0]);print('sin',sin[0]);print('cos',cos[0])
			encoding[:,:,2*k] = sin
			encoding[:,:,2*k+1] = cos

		encoding = self.dropout_f(encoding)
		#encoding = encoding*0 # DUMMY - TESTING
		return encoding
		