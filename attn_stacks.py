from __future__ import print_function
from __future__ import division

import torch
from torch.autograd import Function
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn_utils
import torch.nn as nn
from tinyFlame.models import MLP, DummyModule, generate_tensor_mask_len, count_parameters, dummy_f, get_activation, get_xavier_gain
from attn_layers import SelfAttention_layer
from batch_norms import BatchNormMask, DummyBatchNorm, LayerNorm_proxy, BatchNorm1d_proxy

class CausalConvolution1D_block(nn.Module):
	def __init__(self, input_features, out_features, kernel_size, uses_batchnorm, cnn_blocks, use_residual=True, dropout=0.0):
		super().__init__()

		# ATRIBUTES

		self.B_NORM = DummyBatchNorm
		uses_batchnorm = True
		if uses_batchnorm:
			#B_NORM = nn.InstanceNorm1d
			#self.B_NORM = nn.BatchNorm1d
			#self.B_NORM = BatchNormMask
			#self.B_NORM = LayerNorm_proxy
			self.B_NORM = BatchNorm1d_proxy

		self.input_features = input_features
		self.kernel_size = kernel_size
		self.dropout = dropout
		self.out_features = out_features
		self.use_residual = use_residual
		self.residual_dropout = dropout
		#self.activation_name = 'relu'
		self.activation_name = 'tanh' # da mejor
		input_features_res = input_features

		# MODULES

		self.dropout_f = nn.Dropout(self.dropout)

		self.stack_cnn1d = nn.ModuleList()
		self.bnorms = nn.ModuleList()
		self.functions = []
		for i in range(cnn_blocks):
			cnn1d = nn.Conv1d(in_channels=input_features, out_channels=self.out_features, kernel_size=self.kernel_size)
			torch.nn.init.xavier_uniform_(cnn1d.weight, gain=get_xavier_gain(self.activation_name))
			self.stack_cnn1d.append(cnn1d)

			self.bnorms.append(self.B_NORM(self.out_features, time_last_dim=True))

			f = get_activation(self.activation_name) # NON-LINEAR
			self.functions.append(f)
			self.last_function = f

			input_features = self.out_features
			#print('i:{} - cnn1d params:{}'.format(i,count_parameters(cnn1d)))
		
		if self.use_residual:
			#self.cnn1d_res = nn.Conv1d(in_channels=input_features_res, out_channels=out_features, kernel_size=1)
			#torch.nn.init.xavier_uniform_(self.cnn1d_res.weight, gain=get_xavier_gain('linear'))
			self.cnn1d_res = DummyModule(None)
			self.bnorm_res = self.B_NORM(out_features, time_last_dim=True)
			self.residual_dropout_f = nn.Dropout(self.residual_dropout)
			self.functions[-1] = dummy_f # delete last NON-LINEAR!!

		self.pad = nn.ConstantPad1d([kernel_size-1,0], 0)
		#pool_size = 2
		#self.pool = nn.MaxPool1d(kernel_size=pool_size, stride=1)

	def extra_repr(self):
		b_norm_name = self.bnorms[0].__class__.__name__
		return 'input_features={}, out_features={}, kernel_size={}, stack_cnn1d={}, dropout={}, use_residual={}, residual_dropout={}, activation={}, bnorm={}'.format(
			self.input_features, self.out_features, self.kernel_size, len(self.stack_cnn1d), self.dropout, self.use_residual, self.residual_dropout, self.activation_name, b_norm_name
		)

	def __repr__(self):
		txt = 'CausalConvolution1D_block({})'.format(self.extra_repr())
		txt += '\n\t - Parameters:{}'.format(count_parameters(self))
		txt += '\n\t - non-linears:{}'.format(self.functions)
		txt += '\n\t - last non-linear:{}'.format(self.last_function)
		return txt

	def forward(self, x, mask_T):
		initial_x = x

		for k, (cnn1d,bnorm) in enumerate(zip(self.stack_cnn1d, self.bnorms)):
			x = cnn1d(self.pad(x))
			x = bnorm(x, mask_T)
			hidden_x = x
			x = self.functions[k](x, dim=-1) # NON-LINEAR, if residual last one is dummy
			x = self.dropout_f(x)
			
		if self.use_residual:
			#x = self.residual_dropout_f(x) # ya viene con drop
			initial_x = self.cnn1d_res(initial_x)
			x = initial_x+x # MUY IMPORTANTE
			#x = initial_x # BYPASS the residual
			x = self.bnorm_res(x, mask_T)
			hidden_x = x # save hidden here
			x = self.last_function(x, dim=-1) # last NON-LINEAR
			x = self.dropout_f(x)

		x = x.masked_fill(~mask_T, 0)
		#print('x',x[0,:,0])
		return x, hidden_x

class CausalConvolution1D_stack(nn.Module):
	def __init__(self, input_features, out_features, kernel_sizes, uses_batchnorm=True, cnn_blocks=1, use_residual=True, dropout=0.0):
		super().__init__()

		# ATRIBUTES
		self.input_features = input_features
		self.out_features = out_features
		self.use_residual = use_residual
		self.dropout = dropout
		self.kernel_sizes = kernel_sizes
		self.uses_batchnorm = uses_batchnorm
		BC = CausalConvolution1D_block
		self.uses_sum = True # ayuda a la convergencia

		# MODULES
		
		self.dropout_f = nn.Dropout(self.dropout)

		self.cnn1d_1x1 = nn.Conv1d(in_channels=self.input_features, out_channels=self.out_features, kernel_size=1)
		#self.cnn1d_1x1 = nn.Linear(self.input_features, self.out_features)
		torch.nn.init.xavier_uniform_(self.cnn1d_1x1.weight, gain=get_xavier_gain('linear'))

		self.blocks = nn.ModuleList()
		input_features = self.out_features
		for s in range(len(self.kernel_sizes)):
			block = BC(input_features, self.out_features, self.kernel_sizes[s], self.uses_batchnorm, cnn_blocks, self.use_residual, self.dropout)
			input_features = self.out_features
			self.blocks.append(block)

	def forward(self, x, mask_T):
		#x = x.transpose(-2,-1)
		x = self.cnn1d_1x1(x)
		#x = self.dropout_f(x)
		#x = x.transpose(-2,-1)
		#print('x',x.shape)

		x_holder_list = []
		for block in self.blocks:
			x, hidden_x = block(x, mask_T)
			#print('x',x.shape,'hidden_x',hidden_x.shape)
			x_holder_list.append(hidden_x.unsqueeze(-1))
			#x_holder = x_holder + x

		if self.uses_sum:
			x_holder = torch.cat(x_holder_list, dim=-1).sum(-1)
			#print('x_holder',x_holder.shape)
			x_holder = F.relu(x_holder) # RELU
			return x_holder
		else:
			return x

class AttnStackClassic(nn.Module):
	'''
	here the RNN stack is created with the RNN_cell
	'''
	def __init__(self, input_size, cnn_feat, attn_stacks, dropout, uses_batchnorm, max_curve_length, attn_heads=1, kernel_size=5, along_curve_dropout=0.0):
		super().__init__()

		# ATRIBUTES

		self.cumulated_info = {'grad_norm':{'w':[],'b':[]}}
		self.info = {'grad_norm':{'w':0,'b':0}}
		self.max_curve_length = max_curve_length
		self.uses_batchnorm = uses_batchnorm
		self.dropout = dropout
		self.input_size = input_size
		self.cnn_feat = cnn_feat
		self.kernel_size = kernel_size
		self.attn_stacks = attn_stacks
		self.use_residual = True

		# TEST

		self.cnn_stacks = 3
		self.cnn_blocks = 1
	
		self.attn_stacks = 1
		self.attn_heads = attn_heads
		self.attn_dummy = self.attn_heads<=0

		#attn_heads_list = [1,2,4,8]
		
		#encoding_dim = cnn_feat

		# CNN STACKS

		kernel_sizes = [self.kernel_size]*self.cnn_stacks
		self.ccnn1d_stack = nn.ModuleList()
		ccnn1d = CausalConvolution1D_stack(self.input_size, self.cnn_feat, kernel_sizes, cnn_blocks=self.cnn_blocks,
			uses_batchnorm=self.uses_batchnorm, use_residual=self.use_residual, dropout=self.dropout)
		#ccnn1d = CausalConvolution1D_block(input_size, cnn_feat, kernel_size, uses_batchnorm=uses_batchnorm, cnn_blocks=cnn_blocks)
		self.ccnn1d_stack.append(ccnn1d)

		# ATTN STACKS

		attn_dims = self.cnn_feat
		attn_units_head_list = [attn_dims]*self.attn_stacks
		#print('attn_units_head_list',attn_units_head_list)
		self.selfAttn_stack = nn.ModuleList()
		for k in range(self.attn_stacks):
			#print('self.attn_heads',self.attn_heads)
			if self.attn_dummy:
				print('generating dummy SelfAttention_layer')
				selfAttn = DummyModule(attn_units_head_list[k])
			else:
				selfAttn = SelfAttention_layer(attn_units_head_list[k], self.attn_heads, self.max_curve_length, use_residual=self.use_residual, dropout=self.dropout, along_curve_dropout=along_curve_dropout)
			
			self.selfAttn_stack.append(selfAttn)
			input_size = selfAttn.get_output_dims()
			
			#self.dropout = nn.Dropout(dropout)
			#self.dropout_encoding = nn.Dropout(0.2)
			#self.b_norm = BatchNormMask(cnn_feat)

	def get_output_dims(self):
		return self.selfAttn_stack[-1].get_output_dims()


	def forward(self, x, x_lengths):
		#print('x',x.shape)
		#print('x_lengths',x_lengths.shape,x_lengths[:10])
		#print('encoding',encoding.shape,encoding[0,:,0])
		#print('encoding',encoding.shape,encoding[0,:,1])

		curve_length_mask = generate_tensor_mask_len(x_lengths, x.shape[1], x.device).unsqueeze(-1) # (bxtx1)
		mask_T = curve_length_mask.transpose(-2,-1) # (bx1xt)
		#print('mask',mask.shape,mask[0,:])
		#x[:,:,0] = x[:,:,0]*0 # DUMMY
		
		x = x.transpose(-2,-1)
		for k,ccnn1d in enumerate(self.ccnn1d_stack):
			#print('pre x cnn',x.shape,x[0,:,0])
			
			#print('post x cnn',x.shape,x[0,:,0])
			#print('x',x.shape,x[0])

			#encoding = encoding*0# Kill encoding
			
			#print('x',x[0,0,:])
			#x = self.dropout(x) # not needed?
			#print('drop - x',x[0,0,:])
			#x = x + encoding # just like in transformer model - bad :(
			
			#print('x',x.shape,x[0])

			#encoding = self.auxs[k](encoding)
			#encoding = self.b_norm(encoding, mask)
			#encoding = self.dropout_encoding(encoding*mask)
			#x = torch.cat((x, encoding),dim=-1) # CAT
			#x = self.dropout(x)
			#x = self.auxs[k](x) # FC
			

			#print('new_encoding',new_encoding)
			#x = x + encoding*0

			#x = x.masked_fill(curve_length_mask==0, 0)
			#print('x',x.shape)
			x = ccnn1d(x, mask_T)

		x = x.transpose(-2,-1)

		for k,selfAttn in enumerate(self.selfAttn_stack):
			#print('x',x.shape,x[0])
			res_attn = selfAttn(x, curve_length_mask)
			if self.attn_dummy:
				x = res_attn # ARREGLAR!!!!
			else:
				x, aligments = res_attn
			#print('x',x.shape,x[0])

		return x, self.info

	def capture_grad(self):
		pass

	def capture_grad_end(self):
		pass